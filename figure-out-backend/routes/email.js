var express = require('express');
var router = express.Router();
var nodemailer = require('nodemailer');
var admin = require('firebase-admin');

function createTransporter() {
    return nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'noreply.figureout@gmail.com',
            pass: 'ojownxbsaaojqycl'
        },
        tls: {
            rejectUnauthorized: false
        }
    });
}

router.post('/reminder', (request, response, next) => {
    var transporter = createTransporter()

    admin.auth().listUsers()
        .then(users => {
            var emails = []
            for (let user of users.users) {
                emails.push(user.email)
            }

            const mailOptions = {
                from: 'FigureOut <noreply.figureout@gmail.com>', // sender address
                bcc: emails, // list of receivers
                subject: request.body.subject, // Subject line
                html: request.body.message
            }

            transporter.sendMail(mailOptions, function (err, info) {
                if (err) {
                    response.status(400).json({
                        message: err
                    })
                } else {
                    response.status(201).json(info)
                }
            })
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

module.exports = router;