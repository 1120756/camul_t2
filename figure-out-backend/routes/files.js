var express = require('express');
var router = express.Router();
var storage = require('../utils/storage')
var fs = require('fs');
const { exec } = require("child_process");

var upload = storage.upload;

function execShellCommand(cmd) {
    return new Promise((resolve, reject) => {
        exec(cmd, (error, stdout, stderr) => {
            if (error) {
                console.warn(error);
            }
            resolve(stdout ? stdout : stderr);
        });
    });
}

//uploads a file to the server
router.post('/', upload.single('file'), (request, response, next) => {

    storage.uploadFile('figure-out', './files/' + request.file.filename)
        .then(function () {

            fs.unlinkSync('./files/' + request.file.filename)

            response.status(200).json({
                fileName: request.file.filename
            })
        }).catch(error => {
            response.status(500).json({
                error: "Error uploading media",
                message: error
            })
            fs.unlinkSync('./files/' + request.file.filename)
        })
})

//converts a video to audio and uploads it
router.post('/convert', upload.single('file'), (request, response, next) => {

    var newFilename = request.file.filename.split('.')[0] + '.flac'

    var oldPath = './files/' + request.file.filename
    var newPath = './files/' + newFilename
    //-c:a pcm_s16le 
    var command = "ffmpeg -i " + oldPath + " -ac 1 -ar 16000 " + newPath

    execShellCommand(command)
        .then(function () {
            storage.uploadFile('figure-out', newPath)
                .then(function () {

                    fs.unlinkSync(newPath)
                    fs.unlinkSync(oldPath)

                    response.status(200).json({
                        fileName: newFilename
                    })
                }).catch(error => {
                    response.status(500).json({
                        error: "Error uploading media",
                        message: error
                    })

                    fs.unlinkSync(newPath)
                    fs.unlinkSync(oldPath)
                })
        })
        .catch(error => {
            response.status(500).send(error)
        })
})

//retrieves a file from the server
router.get('/:fileName', (request, response, next) => {
    const fileName = request.params.fileName;

    storage.downloadFile('figure-out', fileName)
        .then(function () {
            response.status(200).sendFile(fileName, { root: "files" }, function (err) {
                if (err) {
                    console.log(err)
                } else {
                    console.log('Sent: ', fileName)
                    fs.unlinkSync('./files/' + request.params.fileName)
                }
            })

        })
        .catch(error => {
            response.status(500).json({
                error: console.error
            })
            fs.unlinkSync('./files/' + request.params.fileName)
        })
})

//deletes a file from the server
router.delete('/:fileName', (request, response, next) => {
    const fileName = request.params.fileName;

    storage.deleteFile('figure-out', fileName)
        .then(function () {
            response.status(200).send("File deleted successfully")
        })
        .catch(error => {
            response.status(500).json({
                error: console.error
            })
        })
})

module.exports = router;