var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Feedback = require('../models/feedback')

//Create a new feedback entry in the DB
router.post('/', (request, response, next) => {

    const feedback = new Feedback({
        _id: new mongoose.Types.ObjectId(),
        type: request.body.type,
        text: request.body.text,
        reviewed: false
    })

    feedback.save()
        .then(res => {
            response.status(201).json({
                id: res.id
            })
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

//retrieve an existing feedback by id
router.get('/:feedbackId', (request, response, next) => {
    const feedbackId = request.params.feedbackId

    Feedback.findById(feedbackId)
        .exec()
        .then(doc => {
            response.status(200).json(doc)
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

//retrieve all feedbacks in the DB
router.get('/', (request, response, next) => {
    Feedback.find()
        .exec()
        .then(doc => {
            response.status(200).json(doc)
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

//update a feedback entry
router.put('/review/:feedbackId', (request, response, next) => {
    const feedbackId = request.params.feedbackId;

    Feedback.findById(feedbackId)
        .exec()
        .then(doc => {
            const feedback = {
                type: doc.type,
                text: doc.text,
                reviewed: request.body.reviewed
            }

            Feedback.findByIdAndUpdate(feedbackId, feedback, { new: true })
                .exec()
                .then(doc => {
                    response.status(200).json(doc)
                })
                .catch(error => {
                    response.status(500).json({
                        error: error
                    })
                })
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

module.exports = router;