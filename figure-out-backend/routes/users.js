var express = require('express');
var router = express.Router();
var admin = require('firebase-admin');
var mongoose = require('mongoose');
const User = require('../models/user');

//criar novo utilizador
router.post('/', (request, response, next) => {
    admin.auth().createUser({
        email: request.body.email,
        emailVerified: false,
        password: request.body.password,
        phoneNumber: request.body.phoneNumber,
        displayName: request.body.displayName,
        photoURL: request.body.photoURL,
        disabled: false
    }).then((userRecord) => {

        const user = new User({
            _id: mongoose.Types.ObjectId(),
            uid: userRecord.uid,
            email: userRecord.email,
            phoneNumber: userRecord.phoneNumber,
            displayName: userRecord.displayName,
            photoURL: userRecord.photoURL,
            birthDate: request.body.birthDate,
            country: request.body.country,
            type: "Tester"
        })

        user.save()
            .then(res => {
                response.status(201).json({
                    uid: userRecord.uid,
                    _id: res._id
                })
            })
            .catch(error => {
                response.status(500).json({
                    error: error
                })
            })

    }).catch(function (error) {
        response.status(500).json({
            error: error
        })
    });
});

//retrieve user by its uid
router.get('/:uid', (request, response, next) => {
    const uid = request.params.uid

    User.find({ uid: uid })
        .exec()
        .then(doc => {
            response.status(200).json(doc)
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})


//retrieve all users
router.get('/', (request, response, next) => {

    User.find()
        .exec()
        .then(doc => {
            response.status(200).json(doc)
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

//update existing user
router.put('/:uid', (request, response, next) => {
    const uid = request.params.uid;

    admin.auth().updateUser(uid, {
        email: request.body.email,
        password: request.body.password,
        phoneNumber: request.body.phoneNumber,
        displayName: request.body.displayName,
        photoURL: request.body.photoURL,
    })
        .then(function (userRecord) {
            const user = {
                email: userRecord.email,
                phoneNumber: userRecord.phoneNumber,
                displayName: userRecord.displayName,
                photoURL: userRecord.photoURL,
                birthDate: request.body.birthDate,
                country: request.body.country,
                type: request.body.type
            }
            User.findOneAndUpdate({ uid: uid }, user, { new: true })
                .exec()
                .then(doc => {
                    response.status(200).json(doc)
                })
                .catch(error => {
                    response.status(500).json({
                        error: error
                    })
                })
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

//Delete an existing user given its id
router.delete('/:uid', (request, response, next) => {
    const uid = request.params.uid;

    admin.auth().deleteUser(uid)
        .then(function () {
            User.findOneAndDelete({ uid: uid })
                .exec()
                .then(doc => {
                    response.status(200).json({
                        deleted: "yes"
                    })
                })
                .catch(error => {
                    response.status(500).json({
                        error: error
                    })
                })
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

module.exports = router;