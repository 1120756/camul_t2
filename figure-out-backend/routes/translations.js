var express = require('express');
var router = express.Router();
var storage = require('../utils/storage');
var fs = require('fs');

const { Translate } = require('@google-cloud/translate').v2;
const vision = require('@google-cloud/vision');
const speech = require('@google-cloud/speech').v1p1beta1;

const translate = new Translate();
const imageAnnotatorClient = new vision.ImageAnnotatorClient();
const speechClient = new speech.SpeechClient()

async function translateText(text, target_language) {
    // Translates the text into the target language. "text" can be a string for
    // translating a single piece of text, or an array of strings for translating
    // multiple texts.

    let [translations] = await translate.translate(text, target_language);
    translations = Array.isArray(translations) ? translations : [translations];

    return translations;
}

async function detectText(fileName) {

    // Performs text detection on the local file
    const [result] = await imageAnnotatorClient.textDetection(fileName);
    const detections = result.textAnnotations;

    var res = detections[0].description;

    return res;
}

async function transcribeAudio(file, origin_language) {
    // The audio file's encoding, sample rate in hertz, and BCP-47 language code

    const audio = {
        uri: 'gs://figure-out/' + file
    };
    const config = {
        encoding: 'FLAC',
        sampleRateHertz: '16000',
        languageCode: origin_language,
    };
    const request = {
        audio: audio,
        config: config,
    };

    // Detects speech in the audio file
    const [response] = await speechClient.recognize(request);
    const transcription = response.results
        .map(result => result.alternatives[0].transcript)
        .join('\n');

    return transcription
}

//text to text translations
router.post('/text', (request, response, next) => {

    const text = request.body.text
    const target_language = request.body.target_language

    translateText(text, target_language)
        .then(res => {
            response.status(201).json({
                translation: res
            })
        })
        .catch(error => {
            console.log(error)
            response.status(500).json({
                error: error
            })
        })
})

//image to text translations
router.post('/image', (request, response, next) => {

    const fileName = request.body.fileName
    const target_language = request.body.target_language

    storage.downloadFile('figure-out', fileName)
        .then(function () {
            detectText('./files/' + fileName)
                .then(res => {
                    translateText(res, target_language)
                        .then(res => {
                            response.status(201).json({
                                translation: res
                            })
                            fs.unlinkSync('./files/' + fileName)
                        })
                        .catch(error => {
                            response.status(500).json({
                                error: error
                            })
                        })
                })
                .catch(error => {
                    response.status(500).json({
                        error: error
                    })
                })
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

//audio to text translations
router.post('/audio', (request, response, next) => {
    const fileName = request.body.fileName
    const origin_language = request.body.origin_language
    const target_language = request.body.target_language

    transcribeAudio(fileName, origin_language)
        .then(res => {
            translateText(res, target_language)
                .then(res => {
                    response.status(201).json({
                        translation: res
                    })
                })
                .catch(error => {
                    response.status(500).json({
                        error: error
                    })
                })
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

module.exports = router;