var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var admin = require('firebase-admin');
var mongoose = require('mongoose');
var serviceAccount = require("./secrets.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://figure-out-4910e.firebaseio.com"
});

var usersRoutes = require('./routes/users');
var translationsRoutes = require('./routes/translations');
var filesRoutes = require('./routes/files');
var diaryRoutes = require('./routes/diary');
var logsRoutes = require('./routes/logs');
var notificationsRoutes = require('./routes/notifications');
var emailRoutes = require('./routes/email');
var feedbackRoutes = require('./routes/feedback');

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
mongoose.connect('mongodb+srv://pedro:Biscoitos123@camul-uomls.gcp.mongodb.net/test?retryWrites=true&w=majority')

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((request, response, next) => {
    response.header('Access-Control-Allow-Origin', '*');
    response.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    response.header('Access-Control-Allow-Headers', '*');
    next();
});

app.use('/users', usersRoutes);
app.use('/translations', translationsRoutes);
app.use('/files', filesRoutes);
app.use('/diary', diaryRoutes);
app.use('/logs', logsRoutes);
app.use('/notifications', notificationsRoutes);
app.use('/email', emailRoutes);
app.use('/feedback', feedbackRoutes);

//if there is no request or response, status is 404
app.use((request, response, next) => {
    const error = new Error('404 not found');
    error.status = 404;
    next(error);
});

//detecting error 500
app.use((error, request, response, next) => {
    response.status(error.status || 500);
    response.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;