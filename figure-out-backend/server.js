var http = require('http');

var app = require('./app');

var port = process.env.PORT || 1337;

process.env.GOOGLE_APPLICATION_CREDENTIALS = 'google.json';

var server = http.createServer(app);

server.listen(port);