const mongoose = require('mongoose');

const feedbackSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    type: String,
    text: String,
    reviewed: Boolean
})

module.exports = mongoose.model('Feedback', feedbackSchema);