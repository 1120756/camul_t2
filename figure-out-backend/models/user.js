const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    uid: String,
    email: String,
    phoneNumber: Number,
    displayName: String,
    photoURL: String,
    birthDate: String,
    country: String,
    type: String
})

module.exports = mongoose.model('User', userSchema);