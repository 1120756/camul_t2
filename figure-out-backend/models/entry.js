const mongoose = require('mongoose');

const entrySchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: String,
    description: String,
    user: String,
    files: Array,
    reviewed: Boolean,
    timestamp: String
})

module.exports = mongoose.model('Entry', entrySchema);