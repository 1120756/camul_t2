const mongoose = require('mongoose');

const logSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    latitude: Number,
    longitude: Number,
    timestamp: Number,
    user: String
})

module.exports = mongoose.model('Log', logSchema);