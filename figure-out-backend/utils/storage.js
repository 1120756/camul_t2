var multer = require('multer');
var crypto = require('crypto');
var { Storage } = require('@google-cloud/storage');

var multerStorage = multer.diskStorage({
    destination: './files',
    filename: (req, file, cb) => {
        let customFileName = crypto.randomBytes(18).toString('hex'),
            fileExtension = file.originalname.split('.')[1] // get file extension from original file name
        cb(null, customFileName + '.' + fileExtension)
    }
})

var upload = multer({ storage: multerStorage })

var storage = new Storage();

async function uploadFile(bucketName, filename) {
    // Uploads a local file to the bucket
    await storage.bucket(bucketName).upload(filename, {
        // Support for HTTP requests made with `Accept-Encoding: gzip`
        gzip: true,
        // By setting the option `destination`, you can change the name of the
        // object you are uploading to a bucket.
        metadata: {
            // Enable long-lived HTTP caching headers
            // Use only if the contents of the file will never change
            // (If the contents will change, use cacheControl: 'no-cache')
            cacheControl: 'public, max-age=31536000',
        },
    })
    console.log(`${filename} uploaded to ${bucketName}.`);
}

async function downloadFile(bucketName, filename) {
    const options = {
        // The path to which the file should be downloaded, e.g. "./file.txt"
        destination: './files/' + filename,
    };

    // Downloads the file
    await storage.bucket(bucketName).file(filename).download(options);
}

async function deleteFile(bucketName, filename) {
    // Deletes the file from the bucket
    await storage.bucket(bucketName).file(filename).delete();
  }

module.exports = {
    uploadFile,
    downloadFile,
    deleteFile,
    upload
}