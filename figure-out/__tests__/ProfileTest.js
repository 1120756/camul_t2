import React, { Component } from 'react';
import { configure } from 'enzyme';
import { shallow } from 'enzyme';
import Secured from '../src/screens/Profile';

import Adapter from 'enzyme-adapter-react-16';


configure({ adapter: new Adapter() });


const createTestProps = (props) => ({
  navigation: {
    navigate: jest.fn(),
    setOptions: jest.fn()
  },
  ...props
});

const createMockFirebase = (firebaseApp) => ({
  auth: jest.fn(),
  ...firebaseApp
});

it("placeholder", () => {

  expect(true);

});


describe("Secured", () => {
  describe("Rendering", () => {
    let wrapper;
    let props;
    let firebaseApp;
    beforeEach(() => {
      props = createTestProps({});
      firebaseApp = {
        auth: jest.fn()
      };

      jest.spyOn(firebaseApp, 'auth').mockImplementation(() => {
        return {
          onAuthStateChanged,
          currentUser: {
            email: 'damientest@figureout.com',
            birthdate: '2020-04-01',
            country: 'Portugal',
            type: 'Administrador',
          },
          getRedirectResult,
          sendPasswordResetEmail
        }
      })

      wrapper = shallow(<Secured {...props} firebaseApp={firebaseApp}/>);
    });


  });

});


function tick() {
   return new Promise(resolve => {
     setTimeout(resolve, 0);
   })
 }

