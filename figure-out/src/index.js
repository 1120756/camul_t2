import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, Switch } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import { firebaseApp } from './firebase';
import './index.css';
import { logUser } from './actions';
import App from './components/App';
import SignIn from './components/SignIn';
import SignUp from './components/SignUp';
import Header from './components/Header';
import SideMenu from './components/SideMenu';
import Profile from './components/Profile';
import EditProfile from './components/EditProfile';
import EditPassword from './components/EditPassword';
import DiaryList from './components/DiaryList';
import UserList from './components/UserList';
import BugList from './components/BugList';
import Statistics from './components/Statistics';
import configureStore from './configureStore';

const store = configureStore();

firebaseApp.auth().onAuthStateChanged(user => {
    if (user) {
        const { uid, email, displayName, photoURL } = user;
        store.dispatch(logUser(uid, email, displayName, photoURL));
    } else {
        store.dispatch(logUser(null, null, null, null));
    }
})

ReactDOM.render(
    <Provider store={store}>
        <Router history={createBrowserHistory()}>
            <Switch>
                <Route exact path="/" render={() => <Header><App /></Header>} />
                <Route path="/signIn" render={() => <Header><SignIn /></Header>} />
                <Route path="/signUp" render={() => <Header><SignUp /></Header>} />
                <Route path="/profile" render={() => <Header><SideMenu><Profile /></SideMenu></Header>} />
                <Route path="/edit" render={() => <Header><SideMenu><EditProfile /></SideMenu></Header>} />
                <Route path="/change-password" render={() => <Header><SideMenu><EditPassword /></SideMenu></Header>} />
                <Route path="/statistics" render={() => <Header><SideMenu><Statistics /></SideMenu></Header>} />
                <Route path="/feedback" render={() => <Header><SideMenu><DiaryList /></SideMenu></Header>} />
                <Route path="/bugs" render={() => <Header><SideMenu><BugList /></SideMenu></Header>} />
                <Route path="/account-promotion" render={() => <Header><SideMenu><UserList /></SideMenu></Header>} />
            </Switch>
        </Router>
    </Provider>, document.getElementById('root')
);
