import { SIGNED_IN, SET_ENTRIES, SET_USERS, SET_BUGS, LOAD_USER, TOGGLE_USER, REVIEW_ENTRY, REVIEW_BUG } from '../constants';

export function logUser(uid, email, displayName, photoURL) {
    const action = {
        type: SIGNED_IN,
        uid,
        email,
        displayName, 
        photoURL
    }

    return action;
}

export function loadUser(user) {
    const action = {
        type: LOAD_USER,
        user
    }

    return action;
}

export function setEntries(entries) {
    const action = {
        type: SET_ENTRIES,
        entries
    }
    
    return action;
}

export function setUsers(users) {
    const action = {
        type: SET_USERS,
        users
    }

    return action;
}

export function toggleUser(uid) {
    const action = {
        type: TOGGLE_USER,
        uid
    }

    return action;
}

export function setBugs(bugs) {
    const action = {
        type: SET_BUGS,
        bugs
    }

    return action;
}

export function reviewEntry(_id) {
    const action = {
        type: REVIEW_ENTRY,
        _id
    }

    return action;
}

export function reviewBug(_id) {
    const action = {
        type: REVIEW_BUG,
        _id
    }

    return action;
}