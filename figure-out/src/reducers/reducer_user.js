import { SIGNED_IN, LOAD_USER } from '../constants';

let user = {
    uid: null,
    email: null,
    password: null,
    displayName: null,
    birthDate: null,
    country: null,
    type: null,
    photoURL: null,
    phoneNumber: null
}

export default (state = user, action) => {
    switch (action.type) {
        case SIGNED_IN:
            const { uid, email, displayName, photoURL } = action;
            user = {
                ...state,
                uid,
                email,
                displayName,
                photoURL,
            }

            return user;

        case LOAD_USER:
            user = action.user;

            return user;
            
        default:
            return state;
    }
}