import { SET_ENTRIES, REVIEW_ENTRY } from '../constants';

export default (state = [], action) => {
    switch(action.type) {
        case SET_ENTRIES:
            const { entries } = action;
            
            return entries;
            
        case REVIEW_ENTRY:
            return state.map(e =>
                {
                    if (e._id !== action._id) {
                        return e;
                    }
    
                    return {
                        ...e,
                        reviewed: !e.reviewed
                    }
                }
            );

        default:
            return state;
    }
}