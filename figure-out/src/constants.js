export const SIGNED_IN = 'SIGNED_IN';
export const SET_ENTRIES = 'SET_ENTRIES';
export const SET_BUGS = 'SET_BUGS';
export const SET_USERS = 'SET_USERS';
export const LOAD_USER = 'LOAD_USER';
export const TOGGLE_USER = 'TOGGLE_USER';
export const REVIEW_ENTRY = 'REVIEW_ENTRY';
export const REVIEW_BUG = 'REVIEW_BUG';

//API
export const API_ADDRESS = 'https://figure-out-backend.herokuapp.com';
export const ANDROID_APP_URL = 'https://exp-shell-app-assets.s3.us-west-1.amazonaws.com/android/%40johannknorr1160996/figure-out-app-a69581fd897842de925dc68ae29fe023-signed.apk';
export const IOS_APP_URL = '';