import { createStore } from 'redux';
import reducer from './reducers';
import { loadState, saveState } from './localStorage';

const configureStore = () => {
    const persistedState = loadState();
    const store = createStore(reducer, persistedState);

    store.subscribe(() => {
        saveState(store.getState());
    });

    return store;
}

export default configureStore;