import * as firebase from 'firebase';

var config = {
    apiKey: "AIzaSyAiVCBlq2w4oj8uVN0DSHp52ksa9Ju98ds",
    authDomain: "figure-out-4910e.firebaseapp.com",
    databaseURL: "https://figure-out-4910e.firebaseio.com",
    projectId: "figure-out-4910e",
    storageBucket: "figure-out-4910e.appspot.com",
    messagingSenderId: "437910704430",
    appId: "1:437910704430:web:af4fa09ddccd882fb32abf",
    measurementId: "G-B9Q6FGKPF4"
};


export const firebaseApp = firebase.initializeApp(config);
export const firebaseAnalitcs = firebase.analytics();
export const firebaseAuthProvider = firebase.auth.EmailAuthProvider;