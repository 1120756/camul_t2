import React, { Component } from 'react';
import { connect } from 'react-redux';
import ProfileTitle from './ProfileTitle';
import { loadUser } from '../actions';
import { API_ADDRESS } from '../constants';

class EditProfile extends Component {

    constructor(props) {
        super(props);
        this.state = { user: {} };

        this.onChangeHandler = this.onChangeHandler.bind(this);
    }

    componentDidMount() {
        this.setState({ user: this.props.user });
    }

    onChangeHandler(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState( prevState => ({
            user: {
                ...prevState.user,
                [name]: value
            }
        }));
    }

    save() {
        const user = this.state.user;
        
        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: user.email,
                phoneNumber: user.phoneNumber,
                displayName: user.displayName,
                photoURL: user.photoURL,
                birthDate: user.birthDate,
                country: user.country,
                type: user.type
            })
        };

        fetch(`${API_ADDRESS}/users/${this.state.user.uid}`, requestOptions)
            .then(response => response.json())
            .then(json => {
                if (this.props.user.email === this.state.user.email)
                {
                    this.props.loadUser(json);
                    var url = window.location.href;
                    var a = document.createElement('a');
                    a.href = url.replace(/edit/gi, 'profile');
                    document.body.appendChild(a); // we need to append the element to the dom -> otherwise it will not work in firefox
                    a.click();
                    a.remove(); //afterwards we remove the element again
                } else {
                    this.props.loadUser(json);
                    var url = window.location.href;
                    var a = document.createElement('a');
                    a.href = url.replace(/edit/gi, 'signin');
                    document.body.appendChild(a); // we need to append the element to the dom -> otherwise it will not work in firefox
                    a.click();
                    a.remove(); //afterwards we remove the element again
                }
            })
            .catch(error => alert(error.message));
    }

    render() {
        return (
            <div style={{paddingRight: '10%'}}>
                <ProfileTitle text="Editar Perfil" />
                <h2 className="profile-subtitle-section">Perfil</h2>
                <div>
                    <div className="profile-data-name" style={{width: '50%', display: 'inline-block'}}>
                        Nome
                    </div>
                    <div className="profile-data" style={{width: '50%', display: 'inline-block'}}>
                        <input name="displayName" value={this.state.user.displayName || ''} type="text" placeholder="Nome" onChange={this.onChangeHandler} />
                    </div>
                </div>
                <hr style={{border: 'solid 0.5px #ebebeb'}} />
                <div>
                    <div className="profile-data-name" style={{width: '50%', display: 'inline-block'}}>
                        Email
                    </div>
                    <div className="profile-data" style={{width: '50%', display: 'inline-block'}}>
                        <input name="email" value={this.state.user.email || ''} type="text" placeholder="Email" onChange={this.onChangeHandler} />
                    </div>
                </div>
                <hr style={{border: 'solid 0.5px #ebebeb'}} />
                <div>
                    <div className="profile-data-name" style={{width: '50%', display: 'inline-block'}}>
                        Data de Nascimento
                    </div>
                    <div className="profile-data" style={{width: '50%', display: 'inline-block'}}>
                        <input name="birthDate" value={this.state.user.birthDate || ''} type="date" placeholder="Data de Nascimento" onChange={this.onChangeHandler} />
                    </div>
                </div>
                <hr style={{border: 'solid 0.5px #ebebeb'}} />
                <div>
                    <div className="profile-data-name" style={{width: '50%', display: 'inline-block'}}>
                        País
                    </div>
                    <div className="profile-data" style={{width: '50%', display: 'inline-block'}}>
                        <input name="country" value={this.state.user.country || ''} type="text" placeholder="País" onChange={this.onChangeHandler} />
                    </div>
                </div>
                <hr style={{border: 'solid 0.5px #ebebeb'}} />
                <div>
                    <div className="profile-data-name" style={{width: '50%', display: 'inline-block'}} />
                    <div className="profile-data" style={{width: '50%', display: 'inline-block'}}>
                        <button
                            type="button"
                            onClick={() => this.save()}
                            style={{backgroundColor: '#16a085', color: 'white', padding: '5px', paddingLeft: '15px', paddingRight: '15px' }}
                        >
                            Guardar
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { user } = state;
    return {
        user
    }
}

export default connect(mapStateToProps, { loadUser })(EditProfile);