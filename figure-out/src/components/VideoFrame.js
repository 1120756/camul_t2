import React, { Component } from 'react';

class VideoFrame extends Component {
    constructor() {
        super();
        this.state = { 
            height: window.innerHeight, 
            width: window.innerWidth,
            count: 1
        };

        this.updateDimensions = this.updateDimensions.bind(this);
    }

    componentDidMount() {
        // Additionally I could have just used an arrow function for the binding `this` to the component...
        window.addEventListener("resize", this.updateDimensions);

        this.updateDimensions();
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    updateDimensions() {
        var width = (window.innerWidth >= 1000) ? window.innerWidth / 2.2 : window.innerWidth - 50;

        var ratio = ((width * 100) / 560) / 100;

        this.setState({
            height: 315 * ratio, 
            width: width
        });
    }

    render() {
        return (
            <iframe
                className="frame"
                width={this.state.width}
                height={this.state.height}
                src="https://www.youtube.com/embed/BgjEA_oTiC0"
                frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture">
            </iframe>
        );
    }
}
  
export default VideoFrame;