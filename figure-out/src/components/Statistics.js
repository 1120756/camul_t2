import React, { Component } from 'react';
import { connect } from 'react-redux';
import ProfileTitle from './ProfileTitle';
import { API_ADDRESS } from '../constants';
import { XYPlot, LineSeries, VerticalBarSeries, XAxis, YAxis } from 'react-vis';

class Statistics extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };

        this.sortGraph = this.sortGraph.bind(this.state);
    }

    componentDidMount() {
        fetch(`${API_ADDRESS}/logs/user/${this.props.user.uid}`)
            .then(response => response.json())
            .then(json => {
                var data = [];
                for (var i = 0; i < json.length; i++) {
                    data.push(new Date(json[i].timestamp));
                }

                data = this.sortGraph(data);

                this.setState({data});
            })
            .catch(error => alert(error.message));
    }

    sortGraph(data) {
        var newData = [];
        var old;
        var counter = 0;

        if (data.length === 0)
            return [];

        data.map((d, index) => {
            d.setMonth(d.getMonth() + 1);

            if (old === undefined)
                old = d;

            if (old !== undefined && (old.getDate() !== d.getDate() || old.getMonth() !== d.getMonth() || old.getFullYear() !== d.getFullYear())) {
                newData.push({ x: old.getDate() + '/' + old.getMonth(), y: counter });
                old = d;
                counter = 1;
            } else {
                counter++;
            }
        });

        newData.push({ x: old.getDate() + '/' + old.getMonth(), y: counter });

        return newData;
    }

    render() {
        return (
            <div style={{paddingRight: '10%'}}>
                <ProfileTitle text="Estatísticas" />
                <div>
                    <h2>Frequência de Login</h2>
                    <XYPlot height={500} width={500} xType="ordinal">
                        <VerticalBarSeries data={this.state.data} />
                        <XAxis title="Data" />
                        <YAxis title="Logs" />
                    </XYPlot>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { user } = state;
    return {
        user
    }
}

export default connect(mapStateToProps, null)(Statistics);