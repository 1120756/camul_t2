import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { firebaseApp } from '../firebase';
import loginImg from '../assets/login-img.png';
import { API_ADDRESS } from '../constants';

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            error: {
                message: ''
            }
        }
    }

    signUp() {
        const { email, password } = this.state;

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ email, password })
        };
        
        fetch(`${API_ADDRESS}/users`, requestOptions)
            .then(response => response.json())
            .then(json => {
                if (json.error) {
                    this.setState({ error: json.error });
                }
                else {
                    firebaseApp.auth().signInWithEmailAndPassword(email, password)
                        .then(() => {
                            var url = window.location.href;
                            var a = document.createElement('a');
                            a.href = url.replace(/signup/gi, 'profile');
                            document.body.appendChild(a); // we need to append the element to the dom -> otherwise it will not work in firefox
                            a.click();
                            a.remove(); //afterwards we remove the element again
                        });
                }
            })
            .catch(error => {
                this.setState({error});
            })
    }

    handleKeyPress = event => {
        if (event.key === 'Enter') {
            this.signUp();
        }
    }

    render() {
        return (
            <div className="login-form">
                <div className="login-form-left">
                    <img src={loginImg} alt="login-img" />
                </div>
                <div className="login-form-right">
                    <h1>Criar conta</h1>
                    <p>Crie uma conta para ter acesso aos dados.</p>
                    <div className="login-form-group">
                        <input
                            type="text"
                            onChange={event => this.setState({ email: event.target.value })}
                            onKeyPress={this.handleKeyPress}
                            placeholder="Email"
                        />
                        <br />
                        <input
                            type="password"
                            onChange={event => this.setState({ password: event.target.value })}
                            onKeyPress={this.handleKeyPress}
                            placeholder="Password"
                        />
                        <div className="div-warning" style={{ display: this.state.error.message === '' ? 'none' : '' }}>{this.state.error.message}</div>
                        <br />
                        <button
                            type="button"
                            onClick={() => this.signUp()}
                            style={{backgroundColor: '#16a085', color: 'white', padding: '5px', paddingLeft: '15px', paddingRight: '15px' }}
                        >
                            Sign Up
                        </button> &nbsp;
                        <div><Link to={'/signin'}>Already a user? Sign in instead</Link></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SignUp;