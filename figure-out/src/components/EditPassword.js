import React, { Component } from 'react';
import ProfileTitle from './ProfileTitle';
import { firebaseApp, firebaseAuthProvider } from '../firebase';

class EditProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPassword: '',
            newPassword: '',
            newPasswordConfirmation: '',
            error: {
                message: ''
            }
        }

        this.onChangeHandler = this.onChangeHandler.bind(this);
    }

    onChangeHandler(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState( prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    save() {
        if (this.state.newPassword === this.state.newPasswordConfirmation && this.state.currentPassword !== this.state.newPassword && this.state.currentPassword !== '' && this.state.newPassword !== '') {
            var user = firebaseApp.auth().currentUser;
            var credential = firebaseAuthProvider.credential(firebaseApp.auth().currentUser.email, this.state.currentPassword)  

            user.reauthenticateWithCredential(credential).then(() => {
                firebaseApp.auth().currentUser.updatePassword(this.state.newPassword);
            }).catch(error => {
                this.setState({error});
            });
        } else {
            if (this.state.currentPassword === '' || this.state.newPassword === '' || this.state.newPasswordConfirmation === '')
                this.setState({ error: { message: 'Existem campos por preencher!' }});
            else
                if (this.state.newPassword === this.state.currentPassword)
                    this.setState({ error: { message: 'Nova password tem de ser diferente da atual!' }});
                else
                    this.setState({ error: { message: 'As passwords inseridas são diferentes!' }});
        }
    }

    render() {
        return (
            <div style={{paddingRight: '10%'}}>
                <ProfileTitle text="Editar Password" />
                <h2 className="profile-subtitle-section">Password</h2>
                <div>
                    <div className="profile-data-name" style={{width: '50%', display: 'inline-block'}}>
                        Password Atual
                    </div>
                    <div className="profile-data" style={{width: '50%', display: 'inline-block'}}>
                        <input name="currentPassword" type="password" onChange={this.onChangeHandler} />
                    </div>
                </div>
                <hr style={{border: 'solid 0.5px #ebebeb;'}} />
                <div>
                    <div className="profile-data-name" style={{width: '50%', display: 'inline-block'}}>
                        Nova Password
                    </div>
                    <div className="profile-data" style={{width: '50%', display: 'inline-block'}}>
                        <input name="newPassword" type="password" onChange={this.onChangeHandler} />
                    </div>
                </div>
                <hr style={{border: 'solid 0.5px #ebebeb;'}} />
                <div>
                    <div className="profile-data-name" style={{width: '50%', display: 'inline-block'}}>
                        Confirme Nova Password
                    </div>
                    <div className="profile-data" style={{width: '50%', display: 'inline-block'}}>
                        <input name="newPasswordConfirmation" type="password" onChange={this.onChangeHandler} />
                    </div>
                </div>
                <hr style={{border: 'solid 0.5px #ebebeb;'}} />
                <div>
                    <div className="profile-data-name" style={{width: '50%', display: 'inline-block'}} />
                    <div className="profile-data" style={{width: '50%', display: 'inline-block'}}>
                        <button
                            type="button"
                            onClick={() => this.save()}
                            style={{backgroundColor: '#16a085', color: 'white', padding: '5px', paddingLeft: '15px', paddingRight: '15px' }}
                        >
                            Guardar
                        </button>
                    </div>
                </div>
                <div style={{width: '100%', color: 'red', display: 'inline-block'}}>
                    <p>{this.state.error.message}</p>
                </div>
            </div>
        )
    }
}

export default EditProfile;