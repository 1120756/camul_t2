import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { firebaseApp } from '../firebase';
import loginImg from '../assets/login-img.png';

class SignIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            error: {
                message: ''
            }
        }
    }

    signIn() {
        const { email, password } = this.state;
        firebaseApp.auth().signInWithEmailAndPassword(email, password)
            .then(() => {
                var url = window.location.href;
                var a = document.createElement('a');
                a.href = url.replace(/signIn/gi, 'profile');
                document.body.appendChild(a); // we need to append the element to the dom -> otherwise it will not work in firefox
                a.click();
                a.remove(); //afterwards we remove the element again
            })
            .catch(error => {
                this.setState({error});
            })
    }

    handleKeyPress = event => {
        if (event.key === 'Enter') {
            this.signIn();
        }
    }

    render() {
        return (
            <div className="login-form">
                <div className="login-form-left">
                    <img src={loginImg} alt="login-img" />
                </div>
                <div className="login-form-right">
                    <h1>Login</h1>
                    <p>Inicie a sessão para ter acesso aos dados.</p>
                    <div className="login-form-group">
                        <input
                            type="text"
                            onChange={event => this.setState({ email: event.target.value })}
                            onKeyPress={this.handleKeyPress}
                            placeholder="Email"
                        />
                        <br />
                        <input
                            type="password"
                            onChange={event => this.setState({ password: event.target.value })}
                            onKeyPress={this.handleKeyPress}
                            placeholder="Password"
                        />
                        <div className="div-warning" style={{ display: this.state.error.message === '' ? 'none' : '' }}>{this.state.error.message}</div>
                        <br />
                        <button
                            type="button"
                            onClick={() => this.signIn()}
                            style={{backgroundColor: '#16a085', color: 'white', padding: '5px', paddingLeft: '15px', paddingRight: '15px' }}
                        >
                            Sign In
                        </button> &nbsp;
                        <div><Link to={'/signup'}>Sign up instead</Link></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SignIn;