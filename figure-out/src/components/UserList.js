import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setUsers } from '../actions';
import UserItem from './UserItem';
import ProfileTitle from './ProfileTitle';
import { API_ADDRESS } from '../constants';

class UserList extends Component {

    componentDidMount() {
        fetch(`${API_ADDRESS}/users`)
            .then(response => response.json())
            .then(json => {
                this.props.setUsers(json);
            })
            .catch(error => alert(error.message));
    }

    render() {
        return (
            <div style={{paddingRight: '10%'}}>
                <ProfileTitle text="Utilizadores" />
                <div>
                    <div className="list-header" style={{width: '50%', display: 'inline-block'}}>Email</div>
                    <div className="list-header" style={{width: '50%', display: 'inline-block'}}></div>
                </div>
                <hr style={{ border: 'solid 0.5px #ebebeb' }} />
                {
                this.props.users.map((user, index) => {
                    return (
                        <UserItem key={index} userSubject={user} />
                    )
                })}
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { users } = state;
    return {
        users
    }
}

export default connect(mapStateToProps, { setUsers })(UserList);