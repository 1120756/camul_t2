import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setBugs, setUsers } from '../actions';
import BugItem from './BugItem';
import ProfileTitle from './ProfileTitle';
import { API_ADDRESS } from '../constants';

class BugList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bugs: []
        };

        this.updateState = this.updateState.bind(this);
    }

    componentDidMount() {
        fetch(`${API_ADDRESS}/feedback`)
            .then(response => response.json())
            .then(json => {
                this.props.setBugs(json);
                this.setState({ bugs: this.props.bugs })
            })
            .catch(error => alert(error.message));
    }

    updateState() {
        var newBugs = this.props.bugs;

        this.setState({ bugs: newBugs });
    }

    render() {
        return (
            <div style={{paddingRight: '10%'}}>
                <ProfileTitle text="Bugs" />
                <div>
                    <div className="list-header" style={{width: '70%', display: 'inline-block'}}>Feedback</div>
                    <div className="list-header" style={{width: '20%', display: 'inline-block'}}>Tipo</div>
                    <div className="list-header" style={{width: '10%', display: 'inline-block'}}></div>
                </div>
                <hr style={{ border: 'solid 0.5px #ebebeb' }} />
                {
                this.state.bugs.map((bug, index) => {
                    return (
                        <BugItem key={index} bug={bug} updateState={this.updateState} />
                    )
                })}
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { bugs, user, users } = state;
    return {
        bugs,
        user,
        users
    }
}

export default connect(mapStateToProps, { setBugs, setUsers })(BugList);