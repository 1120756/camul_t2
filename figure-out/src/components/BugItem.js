import React, { Component } from 'react';
import { connect } from 'react-redux';
import Modal from './Modal';
import { reviewBug } from '../actions';
import { API_ADDRESS } from '../constants';

class BugItem extends Component {

    state = { show: false };

    showModal = () => {
        this.setState({ show: true });
    };

    hideModal = () => {
        this.setState({ show: false });
    };

    handleOnClick() {
        this.props.reviewBug(this.props.bug._id);

        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                reviewed: !this.props.bug.reviewed
            })
        };

        fetch(`${API_ADDRESS}/feedback/review/${this.props.bug._id}`, requestOptions)
            .then(() => {
                this.props.updateState();
            })
            .catch(error => alert(error.message));
    }

    render() {
        var review;
        if (this.props.user.type === 'Administrator' && !this.props.bug.reviewed) {
            review = (
                <button onClick={() => this.handleOnClick()} className="modal-button" style={{ backgroundColor: '#16a085', color: '#ffffff', border: 'solid 3px #16a085' }}>Concluído</button>
            )
        }

        return (
            <div>
                <div className="profile-data-name" style={{ width: '70%', display: 'inline-block' }}>{this.props.bug.text}</div>
                <div className="profile-data-name" style={{ width: '20%', display: 'inline-block' }}>{this.props.bug.type}</div>
                <div style={{ width: '10%', display: 'inline-block', textAlign: 'center' }}>
                    <button
                        style={{backgroundColor: '#16a085', color: 'white', padding: '5px', paddingLeft: '15px', paddingRight: '15px', cursor: 'pointer', marginBottom: '0.001px' }}
                        onClick={this.showModal}
                    >
                        Ver
                    </button>
                </div>
                <hr style={{ border: 'solid 0.5px #ebebeb' }} />
                <Modal show={this.state.show} handleClose={this.hideModal}>
                    <div style={{ padding: '15px'}}>
                        <div className="list-header">Tipo</div>
                        <div className="profile-data-name">{this.props.bug.type}</div>
                    </div>
                    <div style={{ padding: '15px'}}>
                        <div className="list-header">Descrição</div>
                        <div className="profile-data-name" style={{ width: '100%', display: 'inline-block' }}>
                            <p className="text-description" style={{ width: '90%', display: 'inline-block' }}>
                                {this.props.bug.text}
                            </p>
                        </div>
                    </div>
                    <div style={{ padding: '15px' }}><br /><br /><br /><br /><br /><br /><br /></div>
                    <div style={{ textAlign: 'right', padding: '15px' }}>
                        { review }
                        <button onClick={this.hideModal} className="modal-button" style={{ border: 'solid 3px #707070', backgroundColor: '#f4f4f4', color: '#707070' }}>Fechar</button>
                    </div>
                </Modal>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { user } = state;
    return {
        user
    }
}

export default connect(mapStateToProps, { reviewBug })(BugItem);