import React from 'react';
import '../App.css';
import section1 from '../assets/homepage-section1.png';
import smartphone from '../assets/smartphone.png';
import section3 from '../assets/homepage-section3.png';
import DownloadButton from './DownloadButton';
import VideoFrame from './VideoFrame';

function App() {
    var texto = "A Figure Out é uma aplicação \
    móvel que pretende revolucionar o panorama das \
    aplicações tradutoras. Num mundo cada vez mais \
    globalizado, onde existem pessoas a deslocarem-se constantemente \
    para outros países ou até continentes, \
    por vezes a barreira linguístca torna-se quase intransponível. \
    A Figure Out pretende ajudar a resolver este problema, \
    disponibilizando traduções para mais de 100 línguas \
    e em vários formatos, como imagem, texto e áudio."

    return (
        <div>
            <div className="upper-section">
                <img className="homepage-img" src={section1} alt='homepage' />
                <div className="section-content">
                    <p className='homepage-text'><b>Traduções</b><br />em qualquer <b>lugar</b><br />para qualquer <b>idioma</b></p>
                    {/*<a className="homepage-button">Saber mais</a>*/}
                </div>
            </div>
            <div className="upper-section">
                <div className="mid-section-left">
                    <div className="section-content-middle">
                        <p className='homepage-text'>O que é a<br /><b>Figure Out?</b></p>
                        <p className="homepage-explain-text">{texto}</p>
                        {/*<a className="homepage-button" style={{ backgroundColor: '#2fb198' }} href={'/signup'}>Criar conta</a>*/}
                    </div>
                </div>
                <div className="mid-section-right">
                    <VideoFrame />
                </div>
            </div>
            {/*}
            <div className="upper-section">
                <img className="homepage-img" src={section3} alt='homepage' />
            </div>*/}
            <DownloadButton />
        </div>
    )
}

export default App;