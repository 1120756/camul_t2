import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setEntries, setUsers } from '../actions';
import DiaryItem from './DiaryItem';
import ProfileTitle from './ProfileTitle';
import { API_ADDRESS } from '../constants';
import { NavDropdown } from 'react-bootstrap';

class DiaryList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            entries: [],
            filter: 'SHOW_ALL'
        };

        this.onSelectHandler = this.onSelectHandler.bind(this);
        this.updateState = this.updateState.bind(this);
    }

    componentDidMount() {
        if (this.props.user.type === 'Administrator')
            fetch(`${API_ADDRESS}/diary`)
                .then(response => response.json())
                .then(entries => {
                    fetch(`${API_ADDRESS}/users`)
                        .then(response => response.json())
                        .then(users => {
                            this.props.setEntries(entries);
                            this.props.setUsers(users);
                            this.setState({ entries: this.props.entries })
                        })
                        .catch(error => alert(error.message));
                })
                .catch(error => alert(error.message));
        else
            fetch(`${API_ADDRESS}/diary/user/${this.props.user.uid}`)
                .then(response => response.json())
                .then(json => {
                    this.props.setEntries(json);
                })
                .catch(error => alert(error.message));
    }

    updateState() {
        var newEntries = [];
        
        if (this.state.filter === 'SHOW_ALL')
            newEntries = this.props.entries;
        else if (this.state.filter === 'SHOW_ADMIN')
            newEntries = this.props.entries.filter((e) => this.props.users.find((u) => u.uid === e.user).type === 'Administrator');
        else if (this.state.filter === 'SHOW_TESTER')
            newEntries = this.props.entries.filter((e) => this.props.users.find((u) => u.uid === e.user).type === 'Tester');

        this.setState({ entries: newEntries });
    }

    onSelectHandler(key, event) {
        var newEntries = [];

        if (key === 'SHOW_ALL')
            newEntries = this.props.entries;
        else if (key === 'SHOW_ADMIN')
            newEntries = this.props.entries.filter((e) => this.props.users.find((u) => u.uid === e.user).type === 'Administrator');
        else if (key === 'SHOW_TESTER')
            newEntries = this.props.entries.filter((e) => this.props.users.find((u) => u.uid === e.user).type === 'Tester');

        this.setState({ entries: newEntries, filter: key });
    }

    render() {
        var filtro = '';

        if (this.props.user.type === 'Administrator')
            filtro = (
                <div style={{ textAlign: 'right' }}>
                    <NavDropdown onSelect={this.onSelectHandler} alignRight title="Filtro" id="basic-nav-dropdown">
                        <NavDropdown.Item eventKey="SHOW_ALL">Todos</NavDropdown.Item>
                        <NavDropdown.Item eventKey="SHOW_ADMIN">Administrators</NavDropdown.Item>
                        <NavDropdown.Item eventKey="SHOW_TESTER">Testers</NavDropdown.Item>
                    </NavDropdown>
                </div>
            );

        return (
            <div style={{paddingRight: '10%'}}>
                <ProfileTitle text="Diário" />
                { filtro }
                <div>
                    <div className="list-header" style={{width: '45%', display: 'inline-block'}}>Feedback</div>
                    <div className="list-header" style={{width: '45%', display: 'inline-block'}}>Data</div>
                    <div className="list-header" style={{width: '10%', display: 'inline-block'}}></div>
                </div>
                <hr style={{ border: 'solid 0.5px #ebebeb' }} />
                {
                this.state.entries.map((entry, index) => {
                    return (
                        <DiaryItem key={index} entry={entry} updateState={this.updateState} />
                    )
                })}
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { entries, user, users } = state;
    return {
        entries,
        user,
        users
    }
}

export default connect(mapStateToProps, { setEntries, setUsers })(DiaryList);