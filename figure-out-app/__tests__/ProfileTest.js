import React, { Component } from 'react';
import { configure } from 'enzyme';
import { shallow } from 'enzyme';
import Secured from '../src/screens/Profile';

import Adapter from 'enzyme-adapter-react-16';


configure({ adapter: new Adapter() });


const createTestProps = (props) => ({
  navigation: {
    navigate: jest.fn(),
    setOptions: jest.fn()
  },
  ...props
});

const createMockFirebase = (firebaseApp) => ({
  auth: jest.fn(),
  ...firebaseApp
});

it("placeholder", () => {

  expect(true);

});

/**
describe("Secured", () => {
  describe("Rendering", () => {
    let wrapper;
    let props;
    let firebaseApp;
    beforeEach(() => {
      props = createTestProps({});
      firebaseApp = {
        auth: jest.fn()
      };

      jest.spyOn(firebaseApp, 'auth').mockImplementation(() => {
        return {
          onAuthStateChanged,
          currentUser: {
            displayName: 'testDisplayName',
            email: 'test@test.com',
            emailVerified: true
          },
          getRedirectResult,
          sendPasswordResetEmail
        }
      })

      wrapper = shallow(<Secured {...props} firebaseApp={firebaseApp}/>);
    });


    it("Logout methods are called on logout button press", () => {

      const signOutSpy = jest.spyOn(wrapper.instance(), "signOut");
      wrapper.find('TouchableOpacity[name="logoutBtn"]').simulate('press');
      expect(signOutSpy).toHaveBeenCalledTimes(1);

    });

  });

});
*/

function tick() {
   return new Promise(resolve => {
     setTimeout(resolve, 0);
   })
 }

