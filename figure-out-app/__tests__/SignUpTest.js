import React, { Component } from 'react';
import { configure } from 'enzyme';
import { shallow } from 'enzyme';
import SignUp from '../src/screens/SignUp';
import { firebaseApp } from '../firebase';

import Adapter from 'enzyme-adapter-react-16';


configure({ adapter: new Adapter() });


const createTestProps = (props) => ({
  navigation: {
    navigate: jest.fn(),
    setOptions: jest.fn()
  },
  ...props
});


describe("SignUp", () => {
  describe("Rendering", () => {
    let wrapper;
    let props;
    beforeEach(() => {
      props = createTestProps({});
      wrapper = shallow(<SignUp {...props} />);
    });


    it("rendered correctly", () => {
      expect(wrapper.state('email')).toEqual('');
    });

    it("Login methods are called on login button press", () => {

      const signUpSpy = jest.spyOn(wrapper.instance(), "signUp");
      const checkCredentialsSpy = jest.spyOn(wrapper.instance(), "checkCredentials");
      const showErrorMessageSpy = jest.spyOn(wrapper.instance(), "showErrorMessage");

      wrapper.setState({ email: "apptest@figureout.com", password: "figureout" })
      wrapper.find('TouchableOpacity[name="signUpBtn"]').simulate('press');

      expect(checkCredentialsSpy).toHaveBeenNthCalledWith(1, "apptest@figureout.com", "figureout");
      expect(checkCredentialsSpy).toHaveReturned();
      expect(signUpSpy).toHaveBeenCalledTimes(1);
      expect(showErrorMessageSpy).toHaveBeenCalledTimes(0);

    });

  });

});


function tick() {
   return new Promise(resolve => {
     setTimeout(resolve, 0);
   })
 }

