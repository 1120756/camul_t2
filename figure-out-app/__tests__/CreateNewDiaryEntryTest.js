import React, { Component } from 'react';
import { configure } from 'enzyme';
import { shallow } from 'enzyme';
import Secured from '../src/screens/CreateNewDiaryEntry';
import { firebaseApp } from '../firebase';

import Adapter from 'enzyme-adapter-react-16';


configure({ adapter: new Adapter() });


const createTestProps = (props) => ({
  navigation: {
    navigate: jest.fn(),
    setOptions: jest.fn()
  },
  route: jest.fn(),
  ...props
});


describe("CreateNewDiaryEntry", () => {
  describe("Rendering", () => {
    let wrapper;
    let props;
    beforeEach(() => {
      props = createTestProps({});
      wrapper = shallow(<Secured {...props} />);
    });


    it("rendered correctly", () => {
      expect(wrapper.state('feedback_title')).toEqual('');
    });

    it("Input text is cleared", () => {

      const clearSpy = jest.spyOn(wrapper.instance(), "clear");

      wrapper.setState({ feedback_title: "test" })
      expect(wrapper.state('feedback_title')).toBe('test');
      wrapper.find('TouchableOpacity[name="clearBtn"]').simulate('press');

      expect(clearSpy).toHaveBeenCalledTimes(1);
      expect(wrapper.state('feedback_title')).toBe('');

    });

    /** failed to mock firebase
    it("Save methods are called on save button press", () => {

      const saveEntrySpy = jest.spyOn(wrapper.instance(), "saveEntry");

      wrapper.find('TouchableOpacity[name="saveBtn"]').simulate('press');

      expect(saveEntrySpy).toHaveBeenCalledTimes(1);

    });
    */

   it("Delete methods are called on delete button press", () => {

    const deleteEntrySpy = jest.spyOn(wrapper.instance(), "deleteEntry");

    wrapper.setState({ entry: "test" })
    wrapper.find('TouchableOpacity[name="deleteBtn"]').simulate('press');

    expect(deleteEntrySpy).toHaveBeenCalledTimes(1);

  });

  it("Navigates to Diary screen after deleting diary", async () => {

    wrapper.setState({ entry: "test" })
    wrapper.find('TouchableOpacity[name="deleteBtn"]').simulate('press');
    await tick();
    expect(props.navigation.navigate).toHaveBeenNthCalledWith(1, 'Diary');

  });

  
  it("Navigates to RecordAudioScreen screen on audio button press", async () => {

    wrapper.find('TouchableOpacity[name="audioBtn"]').simulate('press');
    await tick();
    expect(props.navigation.navigate).toHaveBeenNthCalledWith(1, 'RecordAudioScreen');

  });



  });

});


function tick() {
   return new Promise(resolve => {
     setTimeout(resolve, 0);
   })
 }

