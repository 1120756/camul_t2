import React, { Component } from 'react';
import { configure } from 'enzyme';
import { shallow } from 'enzyme';
import Secured from '../src/screens/Secured';
import { firebaseApp } from '../firebase';

import Adapter from 'enzyme-adapter-react-16';


configure({ adapter: new Adapter() });


const createTestProps = (props) => ({
  navigation: {
    navigate: jest.fn(),
    setOptions: jest.fn()
  },
  route: {
    params: jest.fn()
  },
  ...props
});

const createImageTestProps = (props) => ({
  navigation: {
    navigate: jest.fn(),
    setOptions: jest.fn()
  },
  route: {
    params: {
      photo: jest.fn()
    }
  },
  ...props
});


describe("Translations", () => {
  describe("Rendering", () => {
    let wrapper;
    let props;
    beforeEach(() => {
      props = createTestProps({});
      wrapper = shallow(<Secured {...props} />);
    });


    it("rendered correctly", () => {
      expect(wrapper.state('selectedInputValue')).toEqual('en');
    });

    it("Input text is cleared", () => {

      const clearSpy = jest.spyOn(wrapper.instance(), "clear");

      wrapper.setState({ intputLangauge: "en", targetLanguage: "pt", inputText: "the" })
      expect(wrapper.state('inputText')).toBe('the');
      wrapper.find('TouchableOpacity[name="clearBtn"]').simulate('press');

      expect(clearSpy).toHaveBeenCalledTimes(1);
      expect(wrapper.state('inputText')).toBe('');

    });

    it("Translate text methods are called", () => {

      const translateSpy = jest.spyOn(wrapper.instance(), "translate");
      const translateTextSpy = jest.spyOn(wrapper.instance(), "translateText");

      wrapper.setState({ intputLangauge: "en", targetLanguage: "pt", inputText: "the" })
      wrapper.find('TouchableOpacity[name="translateBtn"]').simulate('press');

      expect(translateSpy).toHaveBeenCalledTimes(1);
      expect(translateTextSpy).toHaveBeenCalledTimes(1);

    });

    it("Navigates to camera screen on button press", async () => {

      wrapper.find('TouchableOpacity[name="cameraBtn"]').simulate('press');
      await tick();
      expect(props.navigation.navigate).toHaveBeenNthCalledWith(1, 'CameraScreen');


    });


    it("Translate image methods are called", () => {

      props = createImageTestProps({});
      wrapper = shallow(<Secured {...props} />);

      const translateSpy = jest.spyOn(wrapper.instance(), "translate");
      const translateImageSpy = jest.spyOn(wrapper.instance(), "translateImage");

      wrapper.setState({ intputLangauge: "en", targetLanguage: "pt", inputText: "the",  })
      wrapper.find('TouchableOpacity[name="translateBtn"]').simulate('press');

      expect(translateSpy).toHaveBeenCalledTimes(1);
      expect(translateImageSpy).toHaveBeenCalledTimes(1);


    });


  });

});

function tick() {
  return new Promise(resolve => {
    setTimeout(resolve, 0);
  })
}


