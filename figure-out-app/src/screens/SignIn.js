import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  TouchableOpacity,
  Alert,
  KeyboardAvoidingView,
  ScrollView
} from 'react-native';
import { firebaseApp } from '../../firebase';
import i18n from 'i18n-js';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      isLogged: undefined,
      auth: undefined,
      error: {
        message: ''
      },
      fontLoaded: false,
      hasLocationPermission: false,
      location: undefined
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    this.props.navigation.setOptions({
      title: i18n.t('LOGIN'),
      headerTitleStyle: {
        fontFamily: 'Poppins-Regular',
        fontSize: 30,
        padding: 30,
      },

      //headerShown:false
    })
    return (
      <ScrollView style={styles.container}>
        {/*<Text style= {styles.LoginBanner} >Login</Text>*/}
        <View style={styles.inputViewEmail} >
          <TextInput
            style={styles.inputText}
            placeholder="Email"
            autoCorrect={false}
            placeholderTextColor="#aeaeae"
            autoCompleteType="email"
            onChangeText={text => this.setState({ email: text })} />
        </View>
        <View style={styles.inputViewPassword} >
          <TextInput
            style={styles.inputText}
            placeholder="Password"
            placeholderTextColor="#aeaeae"
            autoCompleteType="password"
            autoCorrect={false}
            secureTextEntry={true}
            onChangeText={text => this.setState({ password: text })} />
        </View>
        {/*<TouchableOpacity>
          <Text style={styles.forgot}>{i18n.t('FORGOT_PASSWORD')}</Text>
        </TouchableOpacity>*/}
        <TouchableOpacity name="loginBtn" style={styles.loginBtn} onPress={this.signIn.bind(this)}>
          <Text style={styles.loginText}>{i18n.t('LOGIN')} </Text>
        </TouchableOpacity>
        <TouchableOpacity name="RegisterBtn" style={styles.registerBtn} onPress={() => navigate('SignUpScreen')}>
          <Text style={styles.registerText}>{i18n.t('REGISTER')} </Text>
        </TouchableOpacity>
      </ScrollView>

    );
  }

  async checkCredentials(email, password) {
    var self = this;
    try {
      await firebaseApp.auth().signInWithEmailAndPassword(email, password)
    } catch (error) {
      this.setState({ error });
      //console.log(error.code)
      self.showErrorMessage(this.state.error.code)
      return false;
    };

  }
  async signIn() {

    const { email, password } = this.state;
    await this.checkCredentials(email, password)
  }

  showErrorMessage(code) {

    var text = "";
    //console.log(code)
    switch (code) {
      case "auth/invalid-email":
        text = i18n.t('AUTH_INVALID_EMAIL');
        break;
      case "auth/wrong-password":
        text = i18n.t('AUTH_USER_NOT_FOUND');
        break;
      case "auth/user-not-found":
        text = i18n.t('AUTH_USER_NOT_FOUND');
        break;
      case "auth/too-many-requests":
        text = i18n.t('AUTH_TOO_MANY_REQUESTS');
        break;
    }
    Alert.alert(i18n.t('ERROR'), text);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#31b39a',
  },
  inputViewEmail: {
    marginTop: "28%",
    width: "85%",
    backgroundColor: "#f4f4f4",
    borderRadius: 26,
    height: 52,
    alignSelf:'center'
    //justifyContent: "center",
    //padding: 20
  },
  inputViewPassword: {
    width: "85%",
    backgroundColor: "#f4f4f4",
    borderRadius: 26,
    height: 52,
    marginTop: "10%",
    marginBottom: "20%",
    alignSelf: 'center'
    //justifyContent: "center",
    //padding: 20
  },
  inputText: {
    fontFamily: 'Poppins-Regular',
    marginLeft: 30,
    fontSize: 16,
    //marginTop: 60,
    height: 50,
    color: "#000"
  },
  loginBtn: {
    backgroundColor: "#f4f4f4",
    borderRadius: 41,
    height: 52,
    alignSelf: 'center',
    justifyContent: 'center',
    width: '85%'
  },
  loginText: {
    color: "#000",
    alignSelf: 'center',
    padding: "10%",
    fontFamily: 'Poppins-Regular',
    fontSize: 24
  },
  registerBtn: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: "5%",
  },
  registerText: {
    color: "#fff",
    fontFamily: 'Poppins-Regular',
    textDecorationLine: 'underline',
    alignSelf: 'center',
    padding: "5%",
    fontSize: 26
  },
});