import React, { Component, useState, useEffect } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Picker,
  Alert,
  TouchableHighlight,
  ScrollView
} from 'react-native';
import i18n from 'i18n-js';
import * as Speech from 'expo-speech';
import { TextInput } from 'react-native-gesture-handler';
import { BACKEND_URL } from '../../config'
import { firebaseApp } from '../../firebase';
import { checkConnectivity } from '../CheckConnectivity'
import * as IntentLauncher from 'expo-intent-launcher';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';


export default class ReportBugScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      report_text: "",
      hasFile: false,
      fileName: undefined,
      isImage: false,
      isVideo: false,
      report_type: "bug"
    }
  }

  render() {
    this.props.navigation.setOptions({
      title: i18n.t('REPORT_SCREEN'),
      //Sets Header text of Status Bar
      headerTitleStyle: {
        fontFamily: 'Poppins-Regular',
        fontSize: 30,
      },
    });
    const { navigate } = this.props.navigation
    //console.log(this.props.route.name)
    return (
      <View style={styles.container}>
        <View style={styles.top}>
          <View style={styles.DescriptionToolBar}>
            <View style={styles.PickerBorderAux}>
              <Picker selectedValue={this.state.report_type} style={styles.PickerStyle} onValueChange={(itemValue, itemIndex) => this.setState({ report_type: itemValue})}>
                <Picker.Item label={i18n.t('REPORT_BUG')} value="bug" />
                <Picker.Item label={i18n.t('REPORT_SUGGESTION')} value="suggestion" />
              </Picker>
            </View>
            {/* <TouchableOpacity onPress={this.getPicture.bind(this)}>
              <Image style={styles.smallIcon} source={require('../../assets/ui/image_icon.png')} />
            </TouchableOpacity >
            <TouchableOpacity onPress={this.getVideo.bind(this)}>
              <Image style={styles.smallIcon} source={require('../../assets/ui/video_icon.png')} />
    </TouchableOpacity >*/}
            <TouchableOpacity onPress={this.clear.bind(this)}>
              <Text style={styles.ClearText}>{i18n.t('KEY_SCREEN_CLEAR')}</Text>
            </TouchableOpacity >
          </View>
        </View>
        <View style={styles.middle}>
          <TextInput style={styles.BugTextArea} multiline={true} editable={true} value={this.state.report_text} onChangeText={text => this.setState({ report_text: text })} />
        </View>
        <View style={styles.bottom}>
          <TouchableOpacity style={styles.button} onPress={this.SendFeedback.bind(this)} disabled={this.state.report_text==""}>
            <Text style={styles.btnText}>{i18n.t('SCREEN_DIARY_ENTRY_SAVE')} </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
  clear() {
    this.setState({report_text:"" })
  }

  async SendFeedback(){
    const {navigate} = this.props.navigation
    await fetch(BACKEND_URL + '/feedback', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        type: this.state.report_type,
        text: this.state.report_text,
      }),
    }).then(response => {
      if (response.status === 201) {
        JSON.stringify(response.json().then(function (data) {
          console.log(data)
          navigate("Profile")
        }))
      } else {
        JSON.stringify(response.json().then(function (data) {
          console.log(data)
        }))
      }
    });
  }

  getPicture() {
    var self = this
    ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false
    })
      .then(response => {
        console.log(response)
        if (!response.cancelled) {
          self.setState({ hasFile: true, fileName: response.uri, isImage: true, isVideo: false })
        }
      })
  }
  getVideo() {
    var self = this
    ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Videos,
      allowsEditing: false
    })
      .then(response => {
        console.log(response)
        if (!response.cancelled) {
          self.setState({ hasFile: true, fileName: response.uri, isVideo: true, isImage: false })
        }

      })
  }

  async checkCameraPermission() {
    await Permissions.askAsync(Permissions.CAMERA);
    const status = await Permissions.getAsync(Permissions.CAMERA);
    if (status.status === 'granted') {
      this.setState({ hasCameraPermission: true })
    }

    const rollStatus = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (rollStatus.status !== 'granted') {
      //alert('Sorry, we need camera roll permissions to make this work!');
    } else {
      this.setState({ hasRollPermission: true })
    }

  }

  async componentDidMount() {
    await this.checkCameraPermission();
  }

}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: '#31b39a',
    justifyContent: 'space-between',
    //height: "100%"
  },
  PickerBorderAux: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#fff',
    overflow: 'hidden',
    height: 30,
    width: "40%"
  },
  PickerStyle: {
    backgroundColor: '#fff',
    height: 30,
  },
  TitleText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 25,
    color: "#fff",
  },
  smallIcon: {
    width: 30,
    height: 30,
  },
  top: {
    //flex: 1,
    justifyContent: 'flex-start',
    marginTop: 20,
    //borderWidth: 1,
  },
  middle: {
    //flex: 1,
    marginTop: 10,
    justifyContent: 'flex-start',
    alignSelf: 'center',
    width: "85%",
    height: "70%",
    marginBottom: 10,
    //borderWidth: 1,
  },
  bottom: {
    //flex: 1,
    justifyContent: 'flex-end',
    alignSelf: 'center',
    width: "85%",
    marginBottom: 20,
    //borderWidth: 1,

  },
  BugTextArea: {
    backgroundColor: '#6fcab8',
    borderRadius: 20,
    textAlignVertical: "top",
    width: "100%",
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: "#fff",
    padding: 10,
    height: "100%"
  },
  DescriptionToolBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: "85%",
    alignSelf: 'center',
    //borderWidth: 1,
  },
  button: {
    backgroundColor: "#f4f4f4",
    borderRadius: 41,
    height: 52,
    alignItems: "center",
    justifyContent: "center",
  },
  btnText: {
    color: "#000",
    alignSelf: 'center',
    fontFamily: 'Poppins-Regular',
    fontSize: 30
  },
  smallIconSquare: {
    marginTop: 5,
    width: 30,
    height: 30,
  },
  ClearText: {
    color: "#fff",
    fontFamily: 'Poppins-Regular',
    fontSize: 25,
    height: 30,
  },
  thumbnail: {
    height: 150,
    width: "85%",
    alignSelf: 'center',
    marginBottom: 10,
  },
});