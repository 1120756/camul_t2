import React, { Component, useState, useEffect } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Picker,
  Alert,
  TouchableHighlight
} from 'react-native';
import i18n from 'i18n-js';
import DiaryEntry from '../components/DiaryEntry'
import { getAllEntries, AllEntriesList } from '../model/DiaryEntryList'
import { firebaseApp } from '../../firebase';
import { useIsFocused } from '@react-navigation/native';

class Diary extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }
  render() {
    //console.log(this.props)
    this.props.navigation.setOptions({
      headerShown: false
    })
    var entriesToShow
    if (this.props.isFocused) {
      entriesToShow = AllEntriesList.map((data, i) => {
        return (
          <TouchableOpacity key={i} onPress={() => navigate('NewEntryScreen', { entry: data})}>
            <DiaryEntry key={i} title={data.title} date={data.timestamp} />
          </TouchableOpacity>)
      })
    }
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Text style={styles.welcomeText}>{i18n.t('SCREEN_DIARY')}</Text>
        <View style={styles.line} />
        <View style={styles.FeedbackTitleLine}>
          <Text style={styles.TitleText}>{i18n.t('SCREEN_DIARY_FEEDBACK')}</Text>
          <Text style={styles.DateText}>{i18n.t('SCREEN_DIARY_DATE')}</Text>
        </View>
        <ScrollView style={styles.ScrollStyle}>
          <>{entriesToShow}</>
        </ScrollView>
        <View style={styles.bottom}>
          <TouchableOpacity name="newEntryBtn" onPress={() => navigate('NewEntryScreen')}>
            <Image style={styles.smallIcon} source={require('../../assets/ui/create_entry_icon.png')} />
          </TouchableOpacity >
        </View>
      </View>
    )
  }
}

export default function (props) {
  const isFocused = useIsFocused();
  if (isFocused) {
    getAllEntries(firebaseApp.auth().currentUser.uid)
  }
  return <Diary {...props} isFocused={isFocused} />;
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: '#31b39a',
  },
  line: {
    width: "100%",
    alignSelf: 'center',
    marginTop: 20,
    //marginBottom: 40,
    borderBottomColor: 'white',
    borderBottomWidth: 2,
  },
  ScrollStyle: {
  },
  FeedbackTitleLine: {
    width: "100%",
    marginTop: 45,
    alignSelf: 'center',
    flexDirection: 'row',
    backgroundColor: '#cbece5',
    justifyContent: 'space-between',
    maxHeight: 40,
    height: 40,
  },
  TitleText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 25,
    color: "#000",
    width: "62.5%",
    marginLeft: "7.5%"
  },
  DateText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 25,
    color: "#000",
    width: "30%"
  },
  welcomeText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 40,
    color: "#fff",
    marginTop: 60,
    marginLeft: 40,
    marginBottom: -30,
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
  bottom: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 20,
    marginRight: 20,
    height: 60,
    minHeight: 80
  },
  smallIcon: {
    width: 60,
    height: 60,
    alignSelf: 'flex-end'
  }
});