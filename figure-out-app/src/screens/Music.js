import React, { Component, useState, useEffect } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  ToastAndroid,
  Picker,
  Alert,
  TouchableHighlight,
} from 'react-native';
import i18n from 'i18n-js';
import * as Speech from 'expo-speech';
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import { BACKEND_URL } from '../../config'
import { firebaseApp } from '../../firebase';
import * as ImagePicker from 'expo-image-picker';
import { Video } from 'expo-av';
import * as Permissions from 'expo-permissions';
import * as MediaLibrary from 'expo-media-library';
//import axios from "axios";


// TO DO 

// Verificar  se a musica tem letra
// veerificar quando nao ha musicas

export default class Secured extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedInputValue: 'en',
      selectedOutputValue: 'en',
      targetLanguage: 'en',
      intputLangauge: 'en',
      searchText : '',
      outputText: '',
      fileName: undefined,
      hasFile: false,
      isVideo: false,
      isImage: false,
      songResult: [],
      originalLyric: '',
      showResult: false,
    }
  }
  render() {
    //console.log(this.props)
    this.props.navigation.setOptions({
      headerShown: false
    })
    const { navigate } = this.props.navigation
    return (
      <View style={styles.container}>
        <Text style={styles.welcomeText}>{i18n.t('KEY_SCREEN_SEARCH_SONG')}</Text>
        <View style={styles.line} />
        <View style={styles.TranslationChoosingLine}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.LabelText}>{i18n.t('KEY_SCREEN_TRANSLATE_FROM')}</Text>
            <View style={styles.PickerBorderAux}>
              <Picker selectedValue={this.state.selectedInputValue} style={styles.PickerStyle} onValueChange={(itemValue, itemIndex) => this.setState({ selectedInputValue: itemValue })}>
                <Picker.Item label={i18n.t('ALBANIAN')} value="sq" />
                <Picker.Item label={i18n.t('AMHARIC')} value="am" />
                <Picker.Item label={i18n.t('ARABIC')} value="AR" />
                <Picker.Item label={i18n.t('ARMENIAN')} value="hy" />
                <Picker.Item label={i18n.t('AZERBAIJANI')} value="az" />
                <Picker.Item label={i18n.t('BASQUE')} value="eu" />
                <Picker.Item label={i18n.t('BELARUSIAN')} value="be" />
                <Picker.Item label={i18n.t('BENGALI')} value="bn" />
                <Picker.Item label={i18n.t('BOSNIAN')} value="bs" />
                <Picker.Item label={i18n.t('BULGARIAN')} value="bg" />
                <Picker.Item label={i18n.t('CATALAN')} value="ca" />
                <Picker.Item label={i18n.t('CEBUANO')} value="ceb" />
                <Picker.Item label={i18n.t('CHINESE_SIMPLIFIED')} value="zh" />
                <Picker.Item label={i18n.t('CHINESE_TRADITIONAL')} value="zh-TW" />
                <Picker.Item label={i18n.t('CORSICAN')} value="co" />
                <Picker.Item label={i18n.t('CROATIAN')} value="hr" />
                <Picker.Item label={i18n.t('CZECH')} value="cs" />
                <Picker.Item label={i18n.t('DANISH')} value="da" />
                <Picker.Item label={i18n.t('DUTCH')} value="nl" />
                <Picker.Item label={i18n.t('ENGLISH')} value="en" />
                <Picker.Item label={i18n.t('ESPERANTO')} value="eo" />
                <Picker.Item label={i18n.t('ESTONIAN')} value="et" />
                <Picker.Item label={i18n.t('FINNISH')} value="fi" />
                <Picker.Item label={i18n.t('FRENCH')} value="fr" />
                <Picker.Item label={i18n.t('FRISIAN')} value="fy" />
                <Picker.Item label={i18n.t('GALICIAN')} value="gl" />
                <Picker.Item label={i18n.t('GEORGIAN')} value="ka" />
                <Picker.Item label={i18n.t('GERMAN')} value="de" />
                <Picker.Item label={i18n.t('GREEK')} value="el" />
                <Picker.Item label={i18n.t('GUJARATI')} value="gu" />
                <Picker.Item label={i18n.t('HAITIAN_CREOLE')} value="ht" />
                <Picker.Item label={i18n.t('HAUSA')} value="ha" />
                <Picker.Item label={i18n.t('HAWAIIAN')} value="haw" />
                <Picker.Item label={i18n.t('HEBREW')} value="he" />
                <Picker.Item label={i18n.t('HINDI')} value="hi" />
                <Picker.Item label={i18n.t('HMONG')} value="hmn" />
                <Picker.Item label={i18n.t('HUNGARIAN')} value="hu" />
                <Picker.Item label={i18n.t('ICELANDIC')} value="is" />
                <Picker.Item label={i18n.t('IGBO')} value="ig" />
                <Picker.Item label={i18n.t('INDONESIAN')} value="id" />
                <Picker.Item label={i18n.t('IRISH')} value="ga" />
                <Picker.Item label={i18n.t('ITALIAN')} value="it" />
                <Picker.Item label={i18n.t('JAPANESE')} value="ja" />
                <Picker.Item label={i18n.t('JAVANESE')} value="jv" />
                <Picker.Item label={i18n.t('KANNADA')} value="kn" />
                <Picker.Item label={i18n.t('KAZAKH')} value="kk" />
                <Picker.Item label={i18n.t('KHMER')} value="km" />
                <Picker.Item label={i18n.t('KOREAN')} value="ko" />
                <Picker.Item label={i18n.t('KURDISH')} value="ku" />
                <Picker.Item label={i18n.t('KYRGYZ')} value="ky" />
                <Picker.Item label={i18n.t('LAO')} value="lo" />
                <Picker.Item label={i18n.t('LATIN')} value="la" />
                <Picker.Item label={i18n.t('LATVIAN')} value="lv" />
                <Picker.Item label={i18n.t('LITHUANIAN')} value="lt" />
                <Picker.Item label={i18n.t('LUXEMBOURGISH')} value="lb" />
                <Picker.Item label={i18n.t('MACEDONIAN')} value="mk" />
                <Picker.Item label={i18n.t('MALAGASY')} value="mg" />
                <Picker.Item label={i18n.t('MALAY')} value="ms" />
                <Picker.Item label={i18n.t('MALAYALAM')} value="ml" />
                <Picker.Item label={i18n.t('MALTESE')} value="mt" />
                <Picker.Item label={i18n.t('MAORI')} value="mi" />
                <Picker.Item label={i18n.t('MARATHI')} value="mr" />
                <Picker.Item label={i18n.t('MONGOLIAN')} value="mn" />
                <Picker.Item label={i18n.t('MYANMAR')} value="my" />
                <Picker.Item label={i18n.t('NEPALI')} value="ne" />
                <Picker.Item label={i18n.t('NORWEGIAN')} value="no" />
                <Picker.Item label={i18n.t('NYANJA')} value="ny" />
                <Picker.Item label={i18n.t('PASHTO')} value="ps" />
                <Picker.Item label={i18n.t('PERSIAN')} value="fa" />
                <Picker.Item label={i18n.t('POLISH')} value="pl" />
                <Picker.Item label={i18n.t('PORTUGUESE')} value="pt" />
                <Picker.Item label={i18n.t('PUNJABI')} value="pa" />
                <Picker.Item label={i18n.t('ROMANIAN')} value="ro" />
                <Picker.Item label={i18n.t('RUSSIAN')} value="ru" />
                <Picker.Item label={i18n.t('SAMOAN')} value="sm" />
                <Picker.Item label={i18n.t('SCOTS_GAELIC')} value="gd" />
                <Picker.Item label={i18n.t('SERBIAN')} value="sr" />
                <Picker.Item label={i18n.t('SESOTHO')} value="st" />
                <Picker.Item label={i18n.t('SHONA')} value="st" />
                <Picker.Item label={i18n.t('SINDHI')} value="sd" />
                <Picker.Item label={i18n.t('SINHALA')} value="si" />
                <Picker.Item label={i18n.t('SLOVAK')} value="sk" />
                <Picker.Item label={i18n.t('SLOVENIAN')} value="sl" />
                <Picker.Item label={i18n.t('SOMALI')} value="so" />
                <Picker.Item label={i18n.t('SPANISH')} value="es" />
                <Picker.Item label={i18n.t('SUNDANESE')} value="su" />
                <Picker.Item label={i18n.t('SWAHILI')} value="sw" />
                <Picker.Item label={i18n.t('SWEDISH')} value="sv" />
                <Picker.Item label={i18n.t('TAGALOG')} value="tl" />
                <Picker.Item label={i18n.t('TAJIK')} value="tg" />
                <Picker.Item label={i18n.t('TAMIL')} value="ta" />
                <Picker.Item label={i18n.t('TELUGU')} value="te" />
                <Picker.Item label={i18n.t('THAI')} value="th" />
                <Picker.Item label={i18n.t('TURKISH')} value="tr" />
                <Picker.Item label={i18n.t('UKRANIAN')} value="uk" />
                <Picker.Item label={i18n.t('URDU')} value="ur" />
                <Picker.Item label={i18n.t('UZBEK')} value="uz" />
                <Picker.Item label={i18n.t('VIETNAMESE')} value="vi" />
                <Picker.Item label={i18n.t('WELSH')} value="cy" />
                <Picker.Item label={i18n.t('XHOSA')} value="xh" />
                <Picker.Item label={i18n.t('YIDDISH')} value="yi" />
                <Picker.Item label={i18n.t('YORUBA')} value="yo" />
                <Picker.Item label={i18n.t('ZULU')} value="zu" />
              </Picker>
            </View>
          </View>
          <TouchableOpacity onPress={this.clear.bind(this)}>
            <Text style={styles.ClearText}>{i18n.t('KEY_SCREEN_CLEAR')}</Text>
          </TouchableOpacity >
        </View>

        <View style={styles.TranslationChoosingLine}>
          <View style={{ flexDirection: 'row' }}>
          <TextInput placeholder="Pesquisar música" style={styles.InputText} onChangeText={text => this.setState({ searchText: text })} value={this.state.searchText} />
          </View>
          <TouchableOpacity onPress={this.searchSong.bind(this)}>
            <Text style={styles.SearchText}>{i18n.t('SEARCH_MUSIC_LYRIC_BTN')}</Text>
          </TouchableOpacity >
        </View>
    
        {/* Lista de musicas */}

        {!this.state.showResult && this.state.songResult.map((song) =>
          <Text onPress={this.getLyric.bind(this, song.track.track_id, song.track.has_lyrics)} style={styles.SongListItem}>
            {song.track.artist_name + ' - ' + song.track.track_name} 
          </Text>
        )}

        {this.state.showResult && (
        <ScrollView style={styles.TextArea}>
        <Text style={styles.TranslationText}>
          {this.state.originalLyric}
          </Text>
        </ScrollView>
        )}

        <TouchableOpacity style={styles.TranslateBtn} onPress={this.translate.bind(this)}>
          <Text style={styles.TranslateBtnText}> {i18n.t('KEY_SCREEN_TRANSLATE_ACTION')}</Text>
        </TouchableOpacity>


        <View style={styles.TranslationChoosingLine}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.LabelText}>{i18n.t('KEY_SCREEN_TRANSLATE_TO')}</Text>
            <View style={styles.PickerBorderAux}>
              <Picker selectedValue={this.state.selectedOutputValue} style={styles.PickerStyle} onValueChange={(itemValue, itemIndex) => this.setState({ selectedOutputValue: itemValue, targetLanguage: itemValue, outputText: "" })}>
                <Picker.Item label={i18n.t('AFRIKAANS')} value="af" />
                <Picker.Item label={i18n.t('ALBANIAN')} value="sq" />
                <Picker.Item label={i18n.t('AMHARIC')} value="am" />
                <Picker.Item label={i18n.t('ARABIC')} value="AR" />
                <Picker.Item label={i18n.t('ARMENIAN')} value="hy" />
                <Picker.Item label={i18n.t('AZERBAIJANI')} value="az" />
                <Picker.Item label={i18n.t('BASQUE')} value="eu" />
                <Picker.Item label={i18n.t('BELARUSIAN')} value="be" />
                <Picker.Item label={i18n.t('BENGALI')} value="bn" />
                <Picker.Item label={i18n.t('BOSNIAN')} value="bs" />
                <Picker.Item label={i18n.t('BULGARIAN')} value="bg" />
                <Picker.Item label={i18n.t('CATALAN')} value="ca" />
                <Picker.Item label={i18n.t('CEBUANO')} value="ceb" />
                <Picker.Item label={i18n.t('CHINESE_SIMPLIFIED')} value="zh" />
                <Picker.Item label={i18n.t('CHINESE_TRADITIONAL')} value="zh-TW" />
                <Picker.Item label={i18n.t('CORSICAN')} value="co" />
                <Picker.Item label={i18n.t('CROATIAN')} value="hr" />
                <Picker.Item label={i18n.t('CZECH')} value="cs" />
                <Picker.Item label={i18n.t('DANISH')} value="da" />
                <Picker.Item label={i18n.t('DUTCH')} value="nl" />
                <Picker.Item label={i18n.t('ENGLISH')} value="en" />
                <Picker.Item label={i18n.t('ESPERANTO')} value="eo" />
                <Picker.Item label={i18n.t('ESTONIAN')} value="et" />
                <Picker.Item label={i18n.t('FINNISH')} value="fi" />
                <Picker.Item label={i18n.t('FRENCH')} value="fr" />
                <Picker.Item label={i18n.t('FRISIAN')} value="fy" />
                <Picker.Item label={i18n.t('GALICIAN')} value="gl" />
                <Picker.Item label={i18n.t('GEORGIAN')} value="ka" />
                <Picker.Item label={i18n.t('GERMAN')} value="de" />
                <Picker.Item label={i18n.t('GREEK')} value="el" />
                <Picker.Item label={i18n.t('GUJARATI')} value="gu" />
                <Picker.Item label={i18n.t('HAITIAN_CREOLE')} value="ht" />
                <Picker.Item label={i18n.t('HAUSA')} value="ha" />
                <Picker.Item label={i18n.t('HAWAIIAN')} value="haw" />
                <Picker.Item label={i18n.t('HEBREW')} value="he" />
                <Picker.Item label={i18n.t('HINDI')} value="hi" />
                <Picker.Item label={i18n.t('HMONG')} value="hmn" />
                <Picker.Item label={i18n.t('HUNGARIAN')} value="hu" />
                <Picker.Item label={i18n.t('ICELANDIC')} value="is" />
                <Picker.Item label={i18n.t('IGBO')} value="ig" />
                <Picker.Item label={i18n.t('INDONESIAN')} value="id" />
                <Picker.Item label={i18n.t('IRISH')} value="ga" />
                <Picker.Item label={i18n.t('ITALIAN')} value="it" />
                <Picker.Item label={i18n.t('JAPANESE')} value="ja" />
                <Picker.Item label={i18n.t('JAVANESE')} value="jv" />
                <Picker.Item label={i18n.t('KANNADA')} value="kn" />
                <Picker.Item label={i18n.t('KAZAKH')} value="kk" />
                <Picker.Item label={i18n.t('KHMER')} value="km" />
                <Picker.Item label={i18n.t('KOREAN')} value="ko" />
                <Picker.Item label={i18n.t('KURDISH')} value="ku" />
                <Picker.Item label={i18n.t('KYRGYZ')} value="ky" />
                <Picker.Item label={i18n.t('LAO')} value="lo" />
                <Picker.Item label={i18n.t('LATIN')} value="la" />
                <Picker.Item label={i18n.t('LATVIAN')} value="lv" />
                <Picker.Item label={i18n.t('LITHUANIAN')} value="lt" />
                <Picker.Item label={i18n.t('LUXEMBOURGISH')} value="lb" />
                <Picker.Item label={i18n.t('MACEDONIAN')} value="mk" />
                <Picker.Item label={i18n.t('MALAGASY')} value="mg" />
                <Picker.Item label={i18n.t('MALAY')} value="ms" />
                <Picker.Item label={i18n.t('MALAYALAM')} value="ml" />
                <Picker.Item label={i18n.t('MALTESE')} value="mt" />
                <Picker.Item label={i18n.t('MAORI')} value="mi" />
                <Picker.Item label={i18n.t('MARATHI')} value="mr" />
                <Picker.Item label={i18n.t('MONGOLIAN')} value="mn" />
                <Picker.Item label={i18n.t('MYANMAR')} value="my" />
                <Picker.Item label={i18n.t('NEPALI')} value="ne" />
                <Picker.Item label={i18n.t('NORWEGIAN')} value="no" />
                <Picker.Item label={i18n.t('NYANJA')} value="ny" />
                <Picker.Item label={i18n.t('PASHTO')} value="ps" />
                <Picker.Item label={i18n.t('PERSIAN')} value="fa" />
                <Picker.Item label={i18n.t('POLISH')} value="pl" />
                <Picker.Item label={i18n.t('PORTUGUESE')} value="pt" />
                <Picker.Item label={i18n.t('PUNJABI')} value="pa" />
                <Picker.Item label={i18n.t('ROMANIAN')} value="ro" />
                <Picker.Item label={i18n.t('RUSSIAN')} value="ru" />
                <Picker.Item label={i18n.t('SAMOAN')} value="sm" />
                <Picker.Item label={i18n.t('SCOTS_GAELIC')} value="gd" />
                <Picker.Item label={i18n.t('SERBIAN')} value="sr" />
                <Picker.Item label={i18n.t('SESOTHO')} value="st" />
                <Picker.Item label={i18n.t('SHONA')} value="st" />
                <Picker.Item label={i18n.t('SINDHI')} value="sd" />
                <Picker.Item label={i18n.t('SINHALA')} value="si" />
                <Picker.Item label={i18n.t('SLOVAK')} value="sk" />
                <Picker.Item label={i18n.t('SLOVENIAN')} value="sl" />
                <Picker.Item label={i18n.t('SOMALI')} value="so" />
                <Picker.Item label={i18n.t('SPANISH')} value="es" />
                <Picker.Item label={i18n.t('SUNDANESE')} value="su" />
                <Picker.Item label={i18n.t('SWAHILI')} value="sw" />
                <Picker.Item label={i18n.t('SWEDISH')} value="sv" />
                <Picker.Item label={i18n.t('TAGALOG')} value="tl" />
                <Picker.Item label={i18n.t('TAJIK')} value="tg" />
                <Picker.Item label={i18n.t('TAMIL')} value="ta" />
                <Picker.Item label={i18n.t('TELUGU')} value="te" />
                <Picker.Item label={i18n.t('THAI')} value="th" />
                <Picker.Item label={i18n.t('TURKISH')} value="tr" />
                <Picker.Item label={i18n.t('UKRANIAN')} value="uk" />
                <Picker.Item label={i18n.t('URDU')} value="ur" />
                <Picker.Item label={i18n.t('UZBEK')} value="uz" />
                <Picker.Item label={i18n.t('VIETNAMESE')} value="vi" />
                <Picker.Item label={i18n.t('WELSH')} value="cy" />
                <Picker.Item label={i18n.t('XHOSA')} value="xh" />
                <Picker.Item label={i18n.t('YIDDISH')} value="yi" />
                <Picker.Item label={i18n.t('YORUBA')} value="yo" />
                <Picker.Item label={i18n.t('ZULU')} value="zu" />
              </Picker>
            </View>
            <TouchableOpacity onPress={() => Speech.speak(this.state.outputText, { language: this.state.targetLanguage })} style={{ marginLeft: 10 }}>
              <Image style={styles.smallIcon} source={require('../../assets/ui/triangle_icon_white.png')} />
            </TouchableOpacity >
          </View>
        </View>

        <ScrollView style={styles.TextArea}>
          <Text style={styles.TranslationText}>{this.state.outputText}</Text>
        </ScrollView>
      </View>
    )
  }

  clear() {
    this.setState({ 

      searchText: '', 
      outputText: '', 
      fileName: "", 
      hasFile: false, 
      isVideo: false, 
      isImage: false , 
      songResult : [],
      showResult: false, 
      originalLyric: ''
    
    })
  }


  async getLyric(track_id, has_lyrics) {

    if (has_lyrics === 1)
    {
      await fetch(`http://api.musixmatch.com/ws/1.1/track.lyrics.get?track_id=${track_id}&apikey=64f987f5ff1dc19727201e47c3fbc758`)
          .then(response => response.json())
          .then(json => {
            //console.log(json)
            this.setState({ originalLyric: json.message.body.lyrics.lyrics_body , showResult : true});
          })
          .catch(error => alert(error.message));
    }
    else
    {
      ToastAndroid.show("A música não tem letra disponível!", ToastAndroid.SHORT);
    }
  }

  async searchSong() {

        await fetch(`http://api.musixmatch.com/ws/1.1/track.search?q_track=${this.state.searchText}&page_size=3&page=1&s_track_rating=desc&apikey=64f987f5ff1dc19727201e47c3fbc758`)
                
                .then(response => response.json())
                .then(json => {
                  //console.log(json)
                  this.setState({ songResult:  json.message.body.track_list , showResult : false});
                
                  if(json.message.body.track_list.length === 0)
                  {
                    ToastAndroid.show("Não existe nenhum resultado para esta pesquisa.", ToastAndroid.SHORT);
                  }
                
                })
                .catch(error => alert(error.message));   
    
  }

  async translate() {
    if (this.state.isImage || this.state.isVideo) {
      this.translateMedia()
    } else {
      this.translateText()
    }
  }

  async translateText() {
    var self = this;
    await fetch(BACKEND_URL + '/translations/text', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        target_language: this.state.targetLanguage,
        text: this.state.originalLyric,
        //text: this.state.inputText,
        //artist: this.state.artistText
        //song: this.state.songText
      }),
    }).then(response => {
      if (response.status === 201) {
        JSON.stringify(response.json().then(function (data) {
          console.log(data.translation[0])
          self.setState({ outputText: data.translation[0] })
        }))
      } else {
        JSON.stringify(response.json().then(function (data) {
          console.log(data)
        }))
      }
    });
  }

  async checkCameraPermission() {
    await Permissions.askAsync(Permissions.CAMERA);
    const status = await Permissions.getAsync(Permissions.CAMERA);
    if (status.status === 'granted') {
      this.setState({ hasCameraPermission: true })
    }

    const rollStatus = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (rollStatus.status !== 'granted') {
      //alert('Sorry, we need camera roll permissions to make this work!');
    } else {
      this.setState({ hasRollPermission: true })
    }
  }

  async componentDidMount() {
    await this.checkCameraPermission();
  }


  translateMedia() {
    var self = this
    if (this.state.isVideo) {
      // Alert.alert("Video translation is not currently available")
      const data = self.createFormData(this.state.fileName, { userId: firebaseApp.auth().currentUser.uid })
      console.log(data)
      fetch(BACKEND_URL + '/files/convert', {
        method: 'POST',
        body: data
      }).then(function (response) {
        if (response.ok) {
          return response.json();
        } else {
          return Promise.reject(response);
        }
      }).then(function (res) {
        console.log(res.fileName)
        var format = ""
        if (self.state.isImage) {
          format = "image"
        } else {
          format = "audio"
        }
        fetch(BACKEND_URL + '/translations/' + format, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            target_language: self.state.targetLanguage,
            origin_language: self.state.intputLangauge,
            fileName: res.fileName,
          }),
        }).then(function (response) {
          if (response.ok) {
            JSON.stringify(response.json().then(function (translation) {
              console.log(translation.translation[0])
              self.setState({ outputText: translation.translation[0] })
            }))
          } else {
            return Promise.reject(response);
          }
        })
      })
      return
    }

    var self = this;
    const data = self.createFormData(this.state.fileName, { userId: firebaseApp.auth().currentUser.uid })
    fetch(BACKEND_URL + '/files', {
      method: 'POST',
      body: data
    }).then(function (response) {
      if (response.ok) {
        return response.json();
      } else {
        return Promise.reject(response);
      }
    }).then(function (res) {
      console.log(res.fileName)
      var format = ""
      if (self.state.isImage) {
        format = "image"
      } else {
        format = "video"
      }
      fetch(BACKEND_URL + '/translations/' + format, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          target_language: self.state.targetLanguage,
          origin_language: self.state.intputLangauge,
          fileName: res.fileName,
        }),
      }).then(function (response) {
        if (response.ok) {
          JSON.stringify(response.json().then(function (translation) {
            console.log(translation.translation[0])
            self.setState({ outputText: translation.translation[0] })
          }))
        } else {
          return Promise.reject(response);
        }
      })
    })
  }


  createFormData = (filePath, body) => {
    const data = new FormData();

    if (this.state.isVideo) {
      data.append("file", {
        uri: filePath,
        name: 'image.jpg',
        type: 'image/jpeg'
      });
    } else {
      data.append("file", {
        uri: filePath,
        name: 'video.mp4',
        type: 'video/mp4'
      });
    }
    return data;
  };


  getPicture() {
    var self = this
    ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false
    })
      .then(response => {
        console.log(response)
        if (!response.cancelled) {
          self.setState({ hasFile: true, fileName: response.uri, isImage: true, isVideo: false })
          ImagePicker.getCameraRollPermissionsAsync().then(function () {
            MediaLibrary.saveToLibraryAsync(response.uri)
          })


        }
      })
  }
  getVideo() {
    var self = this
    ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Videos,
      allowsEditing: false
    })
      .then(response => {
        console.log(response)
        if (!response.cancelled) {
          self.setState({ hasFile: true, fileName: response.uri, isVideo: true, isImage: false })
          ImagePicker.getCameraRollPermissionsAsync().then(function () {
            MediaLibrary.saveToLibraryAsync(response.uri)
          })
        }

      })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#31b39a',
  },
  line: {
    width: "85%",
    alignSelf: 'center',
    marginTop: 20,
    //marginBottom: 40,
    borderBottomColor: 'white',
    borderBottomWidth: 2,
  },
  welcomeText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 40,
    color: "#fff",
    marginTop: 60,
    width: '85%',
    alignSelf: 'center',
    alignContent: 'flex-start',
    marginBottom: -30,
  },
  TranslationChoosingLine: {
    marginTop: 20,
    flexDirection: 'row',
    marginBottom: -20,
    maxWidth: "85%",
    alignSelf: 'center',
    width: '85%',
    justifyContent: "space-between",
  },
  PickerBorderAux: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#fff',
    overflow: 'hidden',
    height: 30,
  },
  PickerStyle: {
    backgroundColor: '#fff',
    height: 30,
    width: 100,
  },
  TextArea: {
    backgroundColor: '#6fcab8',
    height: 150,
    borderRadius: 5,
    textAlignVertical: "top",
    width: "85%",
    alignSelf: 'center',
    marginTop: 40,
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: "#fff",
    marginBottom: 10,
  },
  InputText: {
    backgroundColor: '#6fcab8',
    height: 50,
    borderRadius: 5,
    textAlignVertical: "top",
    width: "80%",
    alignSelf: 'center',
    marginTop: 40,
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: "#fff",
    marginBottom: 40,
  },
  TranslationText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: "#fff",
  },
  ImageArea: {
    height: 150,
    width: "85%",
    alignSelf: 'center',
    marginTop: 40,
    marginBottom: 10,
  },
  TranslateBtn: {
    backgroundColor: "#f4f4f4",
    borderRadius: 41,
    height: 52,
    width: "85%",
    alignItems: "center",
    justifyContent: "center",
    alignSelf: 'center'
  },
  TranslateBtnText: {
    color: "#000",
    alignSelf: 'center',
    padding: "5%",
    fontFamily: 'Poppins-Regular',
    fontSize: 24,
    marginTop: 5
  },
  SongListItem: {
    color: "#fff",
    alignSelf: 'auto',
    fontFamily: 'Poppins-Regular',
    fontSize: 20,
    marginTop: 10,
    marginLeft: 30,

  },
  LabelText: {
    color: "#fff",
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
    marginRight: 5,
    height: 30,
  },
  ClearText: {
    color: "#fff",
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
    height: 30,
  },
  SearchText: {
    color: "#fff",
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
    height: 30,
    marginTop: 50,
    marginLeft: 0,
  },
  smallIcon: {
    width: 30,
    height: 30,
  },
  songResult: {
    backgroundColor: '#6fcab8',
    borderRadius: 10,
    margin: 5,
    height: 52,
    width: "85%",
    alignItems: "center",
    justifyContent: "center",
    alignSelf: 'center',
    textAlign: 'left'
  },
});