import React, { Component, useState, useEffect } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Alert,
} from 'react-native';
import i18n from 'i18n-js';
import { Camera } from 'expo-camera';
import * as Permissions from 'expo-permissions';
import { useIsFocused } from '@react-navigation/native';
import { FontAwesome } from '@expo/vector-icons';
import * as MediaLibrary from 'expo-media-library';




class CameraComponent extends Component {

  constructor() {
    super();
  }


  state = {
    hasCameraPermission: false,
    hasRollPermission: false,
    type: Camera.Constants.Type.back,
    ready: false
  }

  async componentDidMount() {
    await this.checkCameraPermission();
  }

  async checkCameraPermission() {
    await Permissions.askAsync(Permissions.CAMERA);
    const status = await Permissions.getAsync(Permissions.CAMERA);
    if (status.status === 'granted') {
      this.setState({ hasCameraPermission: true })
    }

    const rollStatus = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (rollStatus.status !== 'granted') {
      //alert('Sorry, we need camera roll permissions to make this work!');
    } else {
      this.setState({ hasRollPermission: true })
    }

  }

  takePicture = async () => {
    if (this.camera) {
      const {navigate} = this.props.navigation
      let photo = await this.camera.takePictureAsync();
      await MediaLibrary.saveToLibraryAsync(photo.uri);
      //console.log(photo.uri)
      navigate('MainPage', { photo: photo})
    }
  }


  render() {
    const { isFocused } = this.props;
    if (this.state.hasCameraPermission) {
      if (isFocused) {
        return (
          <View style={{ flex: 1 }}>
            <Camera style={{ flex: 1 }} type={this.state.cameraType} ref={ref => { this.camera = ref }}>
              <View style={{ flex: 1, flexDirection: "row", justifyContent: "center", margin: 30 }}>
                <TouchableOpacity
                  name = "cameraBtn"
                  style={{
                    alignSelf: 'flex-end',
                    alignItems: 'center',
                    backgroundColor: 'transparent',
                  }}
                  onPress={() => this.takePicture()}
                >
                  <FontAwesome
                    name="camera"
                    style={{ color: "#fff", fontSize: 40 }}
                  />
                </TouchableOpacity>
              </View>
            </Camera>
          </View>
        )
      } else {
        return (<View />)
      }
    } else {
      return (
        <View style={styles.container}>
          <Text style={styles.welcomeText}>{i18n.t('NO_CAMERA_PERMISSION_WARNING')}</Text>
        </View>
      )
    }
  }
}


export default function (props) {
  const isFocused = useIsFocused();

  return <CameraComponent {...props} isFocused={isFocused} />;
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#31b39a',
    alignItems: 'center',
    justifyContent: 'center',
  },
  welcomeText: {
    fontWeight: "bold",
    fontSize: 30,
    color: "#fff",
    marginBottom: 20,
    alignItems: "center",
    justifyContent: "center",
  }
});