import React, { Component, useState, useEffect } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Picker,
  Alert,
  TouchableHighlight,
} from 'react-native';
import i18n from 'i18n-js';
import * as Speech from 'expo-speech';
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import { BACKEND_URL } from '../../config'
import { firebaseApp } from '../../firebase';
import * as ImagePicker from 'expo-image-picker';
import { Video } from 'expo-av';
import * as Permissions from 'expo-permissions';
import * as MediaLibrary from 'expo-media-library';


export default class Secured extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedInputValue: 'en',
      selectedOutputValue: 'en',
      targetLanguage: 'en',
      intputLangauge: 'en',
      inputText: '',
      outputText: '',
      fileName: undefined,
      hasFile: false,
      isVideo: false,
      isImage: false,
      
    }
  }
  render() {
    //console.log(this.props)
    this.props.navigation.setOptions({
      headerShown: false
    })
    const { navigate } = this.props.navigation
    return (
      <View style={styles.container}>
        <Text style={styles.welcomeText}>{i18n.t('KEY_SCREEN_TRANSLATE')}</Text>
        <View style={styles.line} />
        <View style={styles.TranslationChoosingLine}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.LabelText}>{i18n.t('KEY_SCREEN_TRANSLATE_FROM')}</Text>
            <View style={styles.PickerBorderAux}>
              <Picker selectedValue={this.state.selectedInputValue} style={styles.PickerStyle} onValueChange={(itemValue, itemIndex) => this.setState({ selectedInputValue: itemValue })}>
                <Picker.Item label={i18n.t('ALBANIAN')} value="sq" />
                <Picker.Item label={i18n.t('AMHARIC')} value="am" />
                <Picker.Item label={i18n.t('ARABIC')} value="AR" />
                <Picker.Item label={i18n.t('ARMENIAN')} value="hy" />
                <Picker.Item label={i18n.t('AZERBAIJANI')} value="az" />
                <Picker.Item label={i18n.t('BASQUE')} value="eu" />
                <Picker.Item label={i18n.t('BELARUSIAN')} value="be" />
                <Picker.Item label={i18n.t('BENGALI')} value="bn" />
                <Picker.Item label={i18n.t('BOSNIAN')} value="bs" />
                <Picker.Item label={i18n.t('BULGARIAN')} value="bg" />
                <Picker.Item label={i18n.t('CATALAN')} value="ca" />
                <Picker.Item label={i18n.t('CEBUANO')} value="ceb" />
                <Picker.Item label={i18n.t('CHINESE_SIMPLIFIED')} value="zh" />
                <Picker.Item label={i18n.t('CHINESE_TRADITIONAL')} value="zh-TW" />
                <Picker.Item label={i18n.t('CORSICAN')} value="co" />
                <Picker.Item label={i18n.t('CROATIAN')} value="hr" />
                <Picker.Item label={i18n.t('CZECH')} value="cs" />
                <Picker.Item label={i18n.t('DANISH')} value="da" />
                <Picker.Item label={i18n.t('DUTCH')} value="nl" />
                <Picker.Item label={i18n.t('ENGLISH')} value="en" />
                <Picker.Item label={i18n.t('ESPERANTO')} value="eo" />
                <Picker.Item label={i18n.t('ESTONIAN')} value="et" />
                <Picker.Item label={i18n.t('FINNISH')} value="fi" />
                <Picker.Item label={i18n.t('FRENCH')} value="fr" />
                <Picker.Item label={i18n.t('FRISIAN')} value="fy" />
                <Picker.Item label={i18n.t('GALICIAN')} value="gl" />
                <Picker.Item label={i18n.t('GEORGIAN')} value="ka" />
                <Picker.Item label={i18n.t('GERMAN')} value="de" />
                <Picker.Item label={i18n.t('GREEK')} value="el" />
                <Picker.Item label={i18n.t('GUJARATI')} value="gu" />
                <Picker.Item label={i18n.t('HAITIAN_CREOLE')} value="ht" />
                <Picker.Item label={i18n.t('HAUSA')} value="ha" />
                <Picker.Item label={i18n.t('HAWAIIAN')} value="haw" />
                <Picker.Item label={i18n.t('HEBREW')} value="he" />
                <Picker.Item label={i18n.t('HINDI')} value="hi" />
                <Picker.Item label={i18n.t('HMONG')} value="hmn" />
                <Picker.Item label={i18n.t('HUNGARIAN')} value="hu" />
                <Picker.Item label={i18n.t('ICELANDIC')} value="is" />
                <Picker.Item label={i18n.t('IGBO')} value="ig" />
                <Picker.Item label={i18n.t('INDONESIAN')} value="id" />
                <Picker.Item label={i18n.t('IRISH')} value="ga" />
                <Picker.Item label={i18n.t('ITALIAN')} value="it" />
                <Picker.Item label={i18n.t('JAPANESE')} value="ja" />
                <Picker.Item label={i18n.t('JAVANESE')} value="jv" />
                <Picker.Item label={i18n.t('KANNADA')} value="kn" />
                <Picker.Item label={i18n.t('KAZAKH')} value="kk" />
                <Picker.Item label={i18n.t('KHMER')} value="km" />
                <Picker.Item label={i18n.t('KOREAN')} value="ko" />
                <Picker.Item label={i18n.t('KURDISH')} value="ku" />
                <Picker.Item label={i18n.t('KYRGYZ')} value="ky" />
                <Picker.Item label={i18n.t('LAO')} value="lo" />
                <Picker.Item label={i18n.t('LATIN')} value="la" />
                <Picker.Item label={i18n.t('LATVIAN')} value="lv" />
                <Picker.Item label={i18n.t('LITHUANIAN')} value="lt" />
                <Picker.Item label={i18n.t('LUXEMBOURGISH')} value="lb" />
                <Picker.Item label={i18n.t('MACEDONIAN')} value="mk" />
                <Picker.Item label={i18n.t('MALAGASY')} value="mg" />
                <Picker.Item label={i18n.t('MALAY')} value="ms" />
                <Picker.Item label={i18n.t('MALAYALAM')} value="ml" />
                <Picker.Item label={i18n.t('MALTESE')} value="mt" />
                <Picker.Item label={i18n.t('MAORI')} value="mi" />
                <Picker.Item label={i18n.t('MARATHI')} value="mr" />
                <Picker.Item label={i18n.t('MONGOLIAN')} value="mn" />
                <Picker.Item label={i18n.t('MYANMAR')} value="my" />
                <Picker.Item label={i18n.t('NEPALI')} value="ne" />
                <Picker.Item label={i18n.t('NORWEGIAN')} value="no" />
                <Picker.Item label={i18n.t('NYANJA')} value="ny" />
                <Picker.Item label={i18n.t('PASHTO')} value="ps" />
                <Picker.Item label={i18n.t('PERSIAN')} value="fa" />
                <Picker.Item label={i18n.t('POLISH')} value="pl" />
                <Picker.Item label={i18n.t('PORTUGUESE')} value="pt" />
                <Picker.Item label={i18n.t('PUNJABI')} value="pa" />
                <Picker.Item label={i18n.t('ROMANIAN')} value="ro" />
                <Picker.Item label={i18n.t('RUSSIAN')} value="ru" />
                <Picker.Item label={i18n.t('SAMOAN')} value="sm" />
                <Picker.Item label={i18n.t('SCOTS_GAELIC')} value="gd" />
                <Picker.Item label={i18n.t('SERBIAN')} value="sr" />
                <Picker.Item label={i18n.t('SESOTHO')} value="st" />
                <Picker.Item label={i18n.t('SHONA')} value="st" />
                <Picker.Item label={i18n.t('SINDHI')} value="sd" />
                <Picker.Item label={i18n.t('SINHALA')} value="si" />
                <Picker.Item label={i18n.t('SLOVAK')} value="sk" />
                <Picker.Item label={i18n.t('SLOVENIAN')} value="sl" />
                <Picker.Item label={i18n.t('SOMALI')} value="so" />
                <Picker.Item label={i18n.t('SPANISH')} value="es" />
                <Picker.Item label={i18n.t('SUNDANESE')} value="su" />
                <Picker.Item label={i18n.t('SWAHILI')} value="sw" />
                <Picker.Item label={i18n.t('SWEDISH')} value="sv" />
                <Picker.Item label={i18n.t('TAGALOG')} value="tl" />
                <Picker.Item label={i18n.t('TAJIK')} value="tg" />
                <Picker.Item label={i18n.t('TAMIL')} value="ta" />
                <Picker.Item label={i18n.t('TELUGU')} value="te" />
                <Picker.Item label={i18n.t('THAI')} value="th" />
                <Picker.Item label={i18n.t('TURKISH')} value="tr" />
                <Picker.Item label={i18n.t('UKRANIAN')} value="uk" />
                <Picker.Item label={i18n.t('URDU')} value="ur" />
                <Picker.Item label={i18n.t('UZBEK')} value="uz" />
                <Picker.Item label={i18n.t('VIETNAMESE')} value="vi" />
                <Picker.Item label={i18n.t('WELSH')} value="cy" />
                <Picker.Item label={i18n.t('XHOSA')} value="xh" />
                <Picker.Item label={i18n.t('YIDDISH')} value="yi" />
                <Picker.Item label={i18n.t('YORUBA')} value="yo" />
                <Picker.Item label={i18n.t('ZULU')} value="zu" />
              </Picker>
            </View>
          </View>
          <TouchableOpacity onPress={this.getPicture.bind(this)}>
            <Image style={styles.smallIcon} source={require('../../assets/ui/image_icon.png')} />
          </TouchableOpacity >
          <TouchableOpacity onPress={this.getVideo.bind(this)}>
            <Image style={styles.smallIcon} source={require('../../assets/ui/video_icon.png')} />
          </TouchableOpacity >
          <TouchableOpacity onPress={this.clear.bind(this)}>
            <Text style={styles.ClearText}>{i18n.t('KEY_SCREEN_CLEAR')}</Text>
          </TouchableOpacity >
        </View>
        {!this.state.hasFile && (
          <TextInput style={styles.TextArea} multiline={true} onChangeText={text => this.setState({ inputText: text })} value={this.state.inputText} />
        )}
        {this.state.isImage &&
          <Image style={styles.ImageArea} resizeMode={'contain'} source={{ uri: this.state.fileName }} />
        }
        {this.state.isVideo &&
          <Video source={{ uri: this.state.fileName }} rate={1.0} volume={1.0} isMuted={false} resizeMode={'contain'} shouldPlay isLooping style={styles.ImageArea} />
        }
        <TouchableOpacity style={styles.TranslateBtn} onPress={this.translate.bind(this)}>
          <Text style={styles.TranslateBtnText}> {i18n.t('KEY_SCREEN_TRANSLATE_ACTION')}</Text>
        </TouchableOpacity>
        <View style={styles.TranslationChoosingLine}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.LabelText}>{i18n.t('KEY_SCREEN_TRANSLATE_TO')}</Text>
            <View style={styles.PickerBorderAux}>
              <Picker selectedValue={this.state.selectedOutputValue} style={styles.PickerStyle} onValueChange={(itemValue, itemIndex) => this.setState({ selectedOutputValue: itemValue, targetLanguage: itemValue, outputText: "" })}>
                <Picker.Item label={i18n.t('AFRIKAANS')} value="af" />
                <Picker.Item label={i18n.t('ALBANIAN')} value="sq" />
                <Picker.Item label={i18n.t('AMHARIC')} value="am" />
                <Picker.Item label={i18n.t('ARABIC')} value="AR" />
                <Picker.Item label={i18n.t('ARMENIAN')} value="hy" />
                <Picker.Item label={i18n.t('AZERBAIJANI')} value="az" />
                <Picker.Item label={i18n.t('BASQUE')} value="eu" />
                <Picker.Item label={i18n.t('BELARUSIAN')} value="be" />
                <Picker.Item label={i18n.t('BENGALI')} value="bn" />
                <Picker.Item label={i18n.t('BOSNIAN')} value="bs" />
                <Picker.Item label={i18n.t('BULGARIAN')} value="bg" />
                <Picker.Item label={i18n.t('CATALAN')} value="ca" />
                <Picker.Item label={i18n.t('CEBUANO')} value="ceb" />
                <Picker.Item label={i18n.t('CHINESE_SIMPLIFIED')} value="zh" />
                <Picker.Item label={i18n.t('CHINESE_TRADITIONAL')} value="zh-TW" />
                <Picker.Item label={i18n.t('CORSICAN')} value="co" />
                <Picker.Item label={i18n.t('CROATIAN')} value="hr" />
                <Picker.Item label={i18n.t('CZECH')} value="cs" />
                <Picker.Item label={i18n.t('DANISH')} value="da" />
                <Picker.Item label={i18n.t('DUTCH')} value="nl" />
                <Picker.Item label={i18n.t('ENGLISH')} value="en" />
                <Picker.Item label={i18n.t('ESPERANTO')} value="eo" />
                <Picker.Item label={i18n.t('ESTONIAN')} value="et" />
                <Picker.Item label={i18n.t('FINNISH')} value="fi" />
                <Picker.Item label={i18n.t('FRENCH')} value="fr" />
                <Picker.Item label={i18n.t('FRISIAN')} value="fy" />
                <Picker.Item label={i18n.t('GALICIAN')} value="gl" />
                <Picker.Item label={i18n.t('GEORGIAN')} value="ka" />
                <Picker.Item label={i18n.t('GERMAN')} value="de" />
                <Picker.Item label={i18n.t('GREEK')} value="el" />
                <Picker.Item label={i18n.t('GUJARATI')} value="gu" />
                <Picker.Item label={i18n.t('HAITIAN_CREOLE')} value="ht" />
                <Picker.Item label={i18n.t('HAUSA')} value="ha" />
                <Picker.Item label={i18n.t('HAWAIIAN')} value="haw" />
                <Picker.Item label={i18n.t('HEBREW')} value="he" />
                <Picker.Item label={i18n.t('HINDI')} value="hi" />
                <Picker.Item label={i18n.t('HMONG')} value="hmn" />
                <Picker.Item label={i18n.t('HUNGARIAN')} value="hu" />
                <Picker.Item label={i18n.t('ICELANDIC')} value="is" />
                <Picker.Item label={i18n.t('IGBO')} value="ig" />
                <Picker.Item label={i18n.t('INDONESIAN')} value="id" />
                <Picker.Item label={i18n.t('IRISH')} value="ga" />
                <Picker.Item label={i18n.t('ITALIAN')} value="it" />
                <Picker.Item label={i18n.t('JAPANESE')} value="ja" />
                <Picker.Item label={i18n.t('JAVANESE')} value="jv" />
                <Picker.Item label={i18n.t('KANNADA')} value="kn" />
                <Picker.Item label={i18n.t('KAZAKH')} value="kk" />
                <Picker.Item label={i18n.t('KHMER')} value="km" />
                <Picker.Item label={i18n.t('KOREAN')} value="ko" />
                <Picker.Item label={i18n.t('KURDISH')} value="ku" />
                <Picker.Item label={i18n.t('KYRGYZ')} value="ky" />
                <Picker.Item label={i18n.t('LAO')} value="lo" />
                <Picker.Item label={i18n.t('LATIN')} value="la" />
                <Picker.Item label={i18n.t('LATVIAN')} value="lv" />
                <Picker.Item label={i18n.t('LITHUANIAN')} value="lt" />
                <Picker.Item label={i18n.t('LUXEMBOURGISH')} value="lb" />
                <Picker.Item label={i18n.t('MACEDONIAN')} value="mk" />
                <Picker.Item label={i18n.t('MALAGASY')} value="mg" />
                <Picker.Item label={i18n.t('MALAY')} value="ms" />
                <Picker.Item label={i18n.t('MALAYALAM')} value="ml" />
                <Picker.Item label={i18n.t('MALTESE')} value="mt" />
                <Picker.Item label={i18n.t('MAORI')} value="mi" />
                <Picker.Item label={i18n.t('MARATHI')} value="mr" />
                <Picker.Item label={i18n.t('MONGOLIAN')} value="mn" />
                <Picker.Item label={i18n.t('MYANMAR')} value="my" />
                <Picker.Item label={i18n.t('NEPALI')} value="ne" />
                <Picker.Item label={i18n.t('NORWEGIAN')} value="no" />
                <Picker.Item label={i18n.t('NYANJA')} value="ny" />
                <Picker.Item label={i18n.t('PASHTO')} value="ps" />
                <Picker.Item label={i18n.t('PERSIAN')} value="fa" />
                <Picker.Item label={i18n.t('POLISH')} value="pl" />
                <Picker.Item label={i18n.t('PORTUGUESE')} value="pt" />
                <Picker.Item label={i18n.t('PUNJABI')} value="pa" />
                <Picker.Item label={i18n.t('ROMANIAN')} value="ro" />
                <Picker.Item label={i18n.t('RUSSIAN')} value="ru" />
                <Picker.Item label={i18n.t('SAMOAN')} value="sm" />
                <Picker.Item label={i18n.t('SCOTS_GAELIC')} value="gd" />
                <Picker.Item label={i18n.t('SERBIAN')} value="sr" />
                <Picker.Item label={i18n.t('SESOTHO')} value="st" />
                <Picker.Item label={i18n.t('SHONA')} value="st" />
                <Picker.Item label={i18n.t('SINDHI')} value="sd" />
                <Picker.Item label={i18n.t('SINHALA')} value="si" />
                <Picker.Item label={i18n.t('SLOVAK')} value="sk" />
                <Picker.Item label={i18n.t('SLOVENIAN')} value="sl" />
                <Picker.Item label={i18n.t('SOMALI')} value="so" />
                <Picker.Item label={i18n.t('SPANISH')} value="es" />
                <Picker.Item label={i18n.t('SUNDANESE')} value="su" />
                <Picker.Item label={i18n.t('SWAHILI')} value="sw" />
                <Picker.Item label={i18n.t('SWEDISH')} value="sv" />
                <Picker.Item label={i18n.t('TAGALOG')} value="tl" />
                <Picker.Item label={i18n.t('TAJIK')} value="tg" />
                <Picker.Item label={i18n.t('TAMIL')} value="ta" />
                <Picker.Item label={i18n.t('TELUGU')} value="te" />
                <Picker.Item label={i18n.t('THAI')} value="th" />
                <Picker.Item label={i18n.t('TURKISH')} value="tr" />
                <Picker.Item label={i18n.t('UKRANIAN')} value="uk" />
                <Picker.Item label={i18n.t('URDU')} value="ur" />
                <Picker.Item label={i18n.t('UZBEK')} value="uz" />
                <Picker.Item label={i18n.t('VIETNAMESE')} value="vi" />
                <Picker.Item label={i18n.t('WELSH')} value="cy" />
                <Picker.Item label={i18n.t('XHOSA')} value="xh" />
                <Picker.Item label={i18n.t('YIDDISH')} value="yi" />
                <Picker.Item label={i18n.t('YORUBA')} value="yo" />
                <Picker.Item label={i18n.t('ZULU')} value="zu" />
              </Picker>
            </View>
            <TouchableOpacity onPress={() => Speech.speak(this.state.outputText, { language: this.state.targetLanguage })} style={{ marginLeft: 10 }}>
              <Image style={styles.smallIcon} source={require('../../assets/ui/triangle_icon_white.png')} />
            </TouchableOpacity >
          </View>
        </View>
        <ScrollView style={styles.TextArea}>
          <Text style={styles.TranslationText}>{this.state.outputText}</Text>
        </ScrollView>
      </View>
    )
  }

  clear() {
    this.setState({ inputText: '', outputText: '', fileName: "", hasFile: false, isVideo: false, isImage: false })
  }

  async translate() {
    if (this.state.isImage || this.state.isVideo) {
      this.translateMedia()
    } else {
      this.translateText()
    }
  }

  async translateText() {
    var self = this;
    await fetch(BACKEND_URL + '/translations/text', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        target_language: this.state.targetLanguage,
        text: this.state.inputText,
      }),
    }).then(response => {
      if (response.status === 201) {
        JSON.stringify(response.json().then(function (data) {
          console.log(data.translation[0])
          self.setState({ outputText: data.translation[0] })
        }))
      } else {
        JSON.stringify(response.json().then(function (data) {
          console.log(data)
        }))
      }
    });
  }

  async checkCameraPermission() {
    await Permissions.askAsync(Permissions.CAMERA);
    const status = await Permissions.getAsync(Permissions.CAMERA);
    if (status.status === 'granted') {
      this.setState({ hasCameraPermission: true })
    }

    const rollStatus = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (rollStatus.status !== 'granted') {
      //alert('Sorry, we need camera roll permissions to make this work!');
    } else {
      this.setState({ hasRollPermission: true })
    }
  }

  async componentDidMount() {
    await this.checkCameraPermission();
  }

  

  translateMedia() {
    var self = this
    if (this.state.isVideo) {
      // Alert.alert("Video translation is not currently available")
      const data = self.createFormData(this.state.fileName, { userId: firebaseApp.auth().currentUser.uid })
      console.log(data)
      fetch(BACKEND_URL + '/files/convert', {
        method: 'POST',
        body: data
      }).then(function (response) {
        if (response.ok) {
          return response.json();
        } else {
          return Promise.reject(response);
        }
      }).then(function (res) {
        console.log(res.fileName)
        var format = ""
        if (self.state.isImage) {
          format = "image"
        } else {
          format = "audio"
        }
        fetch(BACKEND_URL + '/translations/' + format, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            target_language: self.state.targetLanguage,
            origin_language: self.state.intputLangauge,
            fileName: res.fileName,
          }),
        }).then(function (response) {
          if (response.ok) {
            JSON.stringify(response.json().then(function (translation) {
              console.log(translation.translation[0])
              self.setState({ outputText: translation.translation[0] })
            }))
          } else {
            return Promise.reject(response);
          }
        })
      })
      return
    }

    var self = this;
    const data = self.createFormData(this.state.fileName, { userId: firebaseApp.auth().currentUser.uid })
    fetch(BACKEND_URL + '/files', {
      method: 'POST',
      body: data
    }).then(function (response) {
      if (response.ok) {
        return response.json();
      } else {
        return Promise.reject(response);
      }
    }).then(function (res) {
      console.log(res.fileName)
      var format = ""
      if (self.state.isImage) {
        format = "image"
      } else {
        format = "video"
      }
      fetch(BACKEND_URL + '/translations/' + format, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          target_language: self.state.targetLanguage,
          origin_language: self.state.intputLangauge,
          fileName: res.fileName,
        }),
      }).then(function (response) {
        if (response.ok) {
          JSON.stringify(response.json().then(function (translation) {
            console.log(translation.translation[0])
            self.setState({ outputText: translation.translation[0] })
          }))
        } else {
          return Promise.reject(response);
        }
      })
    })
  }


  createFormData = (filePath, body) => {
    const data = new FormData();

    if (this.state.isVideo) {
      data.append("file", {
        uri: filePath,
        name: 'image.jpg',
        type: 'image/jpeg'
      });
    } else {
      data.append("file", {
        uri: filePath,
        name: 'video.mp4',
        type: 'video/mp4'
      });
    }
    return data;
  };


  getPicture() {
    var self = this
    ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false
    })
      .then(response => {
        console.log(response)
        if (!response.cancelled) {
          self.setState({ hasFile: true, fileName: response.uri, isImage: true, isVideo: false })
          ImagePicker.getCameraRollPermissionsAsync().then(function () {
            MediaLibrary.saveToLibraryAsync(response.uri)
          })


        }
      })
  }
  getVideo() {
    var self = this
    ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Videos,
      allowsEditing: false
    })
      .then(response => {
        console.log(response)
        if (!response.cancelled) {
          self.setState({ hasFile: true, fileName: response.uri, isVideo: true, isImage: false })
          ImagePicker.getCameraRollPermissionsAsync().then(function () {
            MediaLibrary.saveToLibraryAsync(response.uri)
          })
        }

      })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#31b39a',
  },
  line: {
    width: "85%",
    alignSelf: 'center',
    marginTop: 20,
    //marginBottom: 40,
    borderBottomColor: 'white',
    borderBottomWidth: 2,
  },
  welcomeText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 40,
    color: "#fff",
    marginTop: 60,
    width: '85%',
    alignSelf: 'center',
    alignContent: 'flex-start',
    marginBottom: -30,
  },
  TranslationChoosingLine: {
    marginTop: 20,
    flexDirection: 'row',
    marginBottom: -20,
    maxWidth: "85%",
    alignSelf: 'center',
    width: '85%',
    justifyContent: "space-between",
  },
  PickerBorderAux: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#fff',
    overflow: 'hidden',
    height: 30,
  },
  PickerStyle: {
    backgroundColor: '#fff',
    height: 30,
    width: 100,
  },
  TextArea: {
    backgroundColor: '#6fcab8',
    height: 150,
    borderRadius: 5,
    textAlignVertical: "top",
    width: "85%",
    alignSelf: 'center',
    marginTop: 40,
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: "#fff",
    marginBottom: 10,
  },
  TranslationText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: "#fff",
  },
  ImageArea: {
    height: 150,
    width: "85%",
    alignSelf: 'center',
    marginTop: 40,
    marginBottom: 10,
  },
  TranslateBtn: {
    backgroundColor: "#f4f4f4",
    borderRadius: 41,
    height: 52,
    width: "85%",
    alignItems: "center",
    justifyContent: "center",
    alignSelf: 'center'
  },
  TranslateBtnText: {
    color: "#000",
    alignSelf: 'center',
    padding: "5%",
    fontFamily: 'Poppins-Regular',
    fontSize: 24,
    marginTop: 5
  },
  LabelText: {
    color: "#fff",
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
    marginRight: 5,
    height: 30,
  },
  ClearText: {
    color: "#fff",
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
    height: 30,
  },
  smallIcon: {
    width: 30,
    height: 30,
  },
});