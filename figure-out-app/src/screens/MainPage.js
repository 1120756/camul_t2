import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Text,
} from 'react-native';

export default class MainPage extends Component {

      
    render(){
        return ( 
        <View style={styles.container}>
          <Text style={styles.SplashText}> Figure Out</Text>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#31b39a',
      alignItems: 'center',
      justifyContent: 'center',
    },SplashText: {
      color: "#fff",
      fontSize: 40
    },
  });