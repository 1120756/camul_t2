import React, { Component, useState, useEffect } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    Picker,
    Alert,
    TouchableHighlight
} from 'react-native';
import i18n from 'i18n-js';

export default class DiaryEntry extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Feedback_title: this.props.title,
            Feedback_date: this.props.date,
        }
    }


    render() {
        return (
            <View style={styles.container}>
                <View style={styles.contentLine}>
                    <Text numberOfLines={1} style={styles.TitleText}>{this.state.Feedback_title}</Text>
                    <Text style={styles.DateText}>{this.state.Feedback_date}</Text>
                </View>
                <View style={styles.line} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        //alignContent: 'space-between'
    },
    contentLine: {
        width: "85%",
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 40,
        alignItems: 'center',
    },
    line: {
        width: "100%",
        alignSelf: 'center',
        borderBottomColor: 'white',
        borderBottomWidth: 1,
    },
    TitleText: {
        fontFamily: 'Poppins-Regular',
        fontSize: 20,
        color: "#fff",
        width: "50%"
    },
    DateText: {
        fontFamily: 'Poppins-Regular',
        fontSize: 20,
        color: "#fff",
        textAlign: 'center',

    },
    smallIcon: {
        width: 30,
        height: 30,
    }
});