import { checkConnectivity } from '../CheckConnectivity'
import { BACKEND_URL } from '../../config'
import { AsyncStorage, Alert } from 'react-native';
import { firebaseApp } from '../../firebase';


export var AllEntriesList = [];
export var EntriesToSendList = [];

export async function pushNewEntry(entry, uid) {
    EntriesToSendList.push(entry)
    var isConnected = await checkConnectivity()
    if (isConnected) {
        sendEntries(uid)
    } else {
        console.log("Saving Entries Locally")
        saveEntriesLocally(uid)
    }
}
export async function putEntry(entry) {

    fetch(BACKEND_URL + '/diary/' + entry._id, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            title: entry.title,
            description: entry.description,
            timestamp: entry.timestamp
        }),
    }).then(response => {
        if (response.status === 200) {
            JSON.stringify(response.json().then(function (data) {
                console.log(data)
            }))
        } else {
            console.log("RESTORED")
        }
    });
}

export async function updateLocalEntry(oldEntry, newEntry, uid) {
    AsyncStorage.getItem('@figureout:unsentEntries/' + uid).then(function (value) {
        addItemsToList(EntriesToSendList, JSON.parse(value))
    }).then(function () {
        for (var i = 0; i < EntriesToSendList.length; i++) {
            if (JSON.stringify(oldEntry) == JSON.stringify(EntriesToSendList[i])) {
                console.log("FOUND")
                EntriesToSendList[i]=newEntry
                saveEntriesLocally(uid)
                break;
            }
        }
        for (var i = 0; i < AllEntriesList.length; i++) {
            if (JSON.stringify(oldEntry) == JSON.stringify(AllEntriesList[i])) {
                console.log("FOUND AGAIN")
                AllEntriesList[i]=newEntry;
                break;
            }
        }
    })
}

async function sendEntries(uid) {
    var self = this;
    AsyncStorage.getItem('@figureout:unsentEntries/' + uid).then(function (value) {
        addItemsToList(EntriesToSendList, JSON.parse(value))
    }).then(function () {
        EntriesToSendList.forEach(entry => {
            var index = -1
            for (var i = 0; i < EntriesToSendList.length; i++) {
                if (JSON.stringify(entry) == JSON.stringify(EntriesToSendList[i])) {
                    index = i;
                    break;
                }
            }
            if (index > -1) {
                EntriesToSendList.splice(index, 1);
                saveEntriesLocally(uid)
            }
            fetch(BACKEND_URL + '/diary', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    user: entry.user,
                    title: entry.title,
                    description: entry.description,
                    timestamp: entry.timestamp
                }),
            }).then(response => {
                if (response.status === 201) {
                    JSON.stringify(response.json().then(function (data) {
                        console.log(data)
                    }))
                } else {
                    console.log("RESTORED", entry.title)
                    EntriesToSendList.push(entry)
                }
            });
        });
    })
}

export async function saveEntriesLocally(uid) {
    await AsyncStorage.setItem('@figureout:unsentEntries/' + uid, JSON.stringify(EntriesToSendList));
    console.log("Stored Locally")

}

export async function getAllEntries(uid) {
    checkConnectivity().then(function (isConnected) {
        if (isConnected) {
            sendEntries(uid).then(function () {
                fetch(BACKEND_URL + '/diary/user/' + uid, {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                }).then(response => {
                    if (response.status === 200) {
                        JSON.stringify(response.json().then(function (data) {
                            console.log(data)
                            addItemsToList(AllEntriesList, data)
                            AsyncStorage.getItem('@figureout:unsentEntries/' + uid).then(function (value) {
                                addItemsToList(AllEntriesList, JSON.parse(value))
                            })

                        }))
                    } else {
                        JSON.stringify(response.json().then(function (data) {
                            console.log(data)
                        }))
                    }
                });
            })

        } else {
            console.log("Retrieving Local Data")
            AsyncStorage.getItem('@figureout:unsentEntries/' + uid).then(function (value) {
                addItemsToList(AllEntriesList, JSON.parse(value))
            })
        }
    })
}


function addItemsToList(list, items) {
    if (items) {
        for (var i = 0; i < items.length; i++) {
            var exists = false;
            for (var j = 0; j < list.length; j++) {
                if (JSON.stringify(list[j]) == JSON.stringify(items[i])) {
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                list.push(items[i])
            }
        }
    }
}

export async function DeleteEntry(entry) {
    if (entry._id) {
        fetch(BACKEND_URL + '/diary/' + entry._id, {
            method: 'DELETE',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(response => {
            if (response.status === 200) {
                JSON.stringify(response.json().then(function (data) {
                    for (var i = 0; i < AllEntriesList.length; i++) {
                        if (JSON.stringify(entry) == JSON.stringify(AllEntriesList[i])) {
                            AllEntriesList.splice(i, 1);
                            break;
                        }
                    }
                    return true
                }))
            } else {
                return false
            }
        });
    } else {
        AsyncStorage.getItem('@figureout:unsentEntries/' + entry.user).then(function (value) {
            addItemsToList(EntriesToSendList, JSON.parse(value))
        }).then(function () {
            for (var i = 0; i < EntriesToSendList.length; i++) {
                if (JSON.stringify(entry) == JSON.stringify(EntriesToSendList[i])) {
                    EntriesToSendList.splice(i, 1);
                    saveEntriesLocally(uid)
                    break;
                }
            }
            for (var i = 0; i < AllEntriesList.length; i++) {
                if (JSON.stringify(entry) == JSON.stringify(AllEntriesList[i])) {
                    AllEntriesList.splice(i, 1);
                    break;
                }
            }
        })
        return true
    }
}