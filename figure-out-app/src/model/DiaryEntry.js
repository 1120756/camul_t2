class DiaryEntry{
    constructor(user, title, text, files, timestamp){
        this.user = user,
        this.title = title,
        this.description = text,
        this.files = files
        this.timestamp = timestamp
    }
}

export default DiaryEntry;