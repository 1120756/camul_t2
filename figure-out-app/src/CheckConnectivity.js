
import * as Network from 'expo-network';

export async function checkConnectivity() {
    //return false
    var connected = await Network.getNetworkStateAsync()
    /*if (connected.type == "WIFI") {
        return connected.isInternetReachable;
    }
    if (connected.type != "CELLULAR") {
        return connected.isInternetReachable;
    }*/
    return connected.isInternetReachable;
}
