## Available Scripts

### Initial Setup

 * Clone the project : `git clone https://bitbucket.org/RicGD/camul1920.git`
 * Install the Expo CLI: `npm install -g expo-cli`
 * Update npm packages: ``npm install`

### Start the app

* Start the app: `expo start`
  * Runs the app in the development mode.
  * Open [http://localhost:19002/](http://localhost:19002/) to view the Expo Developer Tools in the browser.
  * Press "Run in web browser" to see the application in your web browser.
  * Alternatively read the QR code with the Expo app on a mobile device in the same network.
  * The page will reload if  edits are made.
  * Any errors will be presented by the Expo Developer Tools page
* Stop the app: `ctrl + c`

### Useful links

* https://reactnative.dev/docs/getting-started

