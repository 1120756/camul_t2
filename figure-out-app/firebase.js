import * as firebase from 'firebase/app';
//import 'firebase/analytics';
import 'firebase/auth'

var config = {
    apiKey: "AIzaSyAiVCBlq2w4oj8uVN0DSHp52ksa9Ju98ds",
    authDomain: "figure-out-4910e.firebaseapp.com",
    databaseURL: "https://figure-out-4910e.firebaseio.com",
    projectId: "figure-out-4910e",
    storageBucket: "figure-out-4910e.appspot.com",
    messagingSenderId: "437910704430",
    appId: "1:437910704430:web:1212dfabc3a4c9cdb32abf",
    measurementId: "G-745LN2XYFF"
};


export const firebaseApp = firebase.initializeApp(config);
//export const firebaseAnalitcs = firebase.analytics(); //TODO - Check for EXPO SDK 37
//export const goalRef = firebase.database().ref('goals');
//export const completeGoalRef = firebase.database().ref('completeGoals');