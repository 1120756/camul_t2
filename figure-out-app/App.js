
import * as React from 'react';

import {
  Image,
  StyleSheet,
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import SignIn from './src/screens/SignIn';
import Secured from './src/screens/Secured';
import SignUp from './src/screens/SignUp';
import WelcomePage from './src/screens/MainPage';
import Diary from './src/screens/Diary';
import Profile from './src/screens/Profile';
import CameraScreen from './src/screens/Camera'
import NewEntryScreen from './src/screens/CreateNewDiaryEntry';
import ReportBugScreen from './src/screens/ReportBugScreen';
import Music from './src/screens/Music';

import * as Localization from 'expo-localization';
import { translations } from './locale'
import i18n from 'i18n-js';
import * as Font from 'expo-font'
import { firebaseApp } from './firebase';
import { getAllEntries } from './src/model/DiaryEntryList'
import * as Location from 'expo-location';
import { BACKEND_URL } from './config'
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import { Notifications } from 'expo';
import RecordAudioScreen from './src/screens/RecordAudioScreen'
import AboutScreen from './src/screens/AboutScreen'


i18n.fallbacks = true;
i18n.defaultLocale = "en-GB"
i18n.locale = Localization.locale;
i18n.translations = translations;



const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const tabIcons = {
  profile_true: require('./assets/ui/profile_true_icon.png'),
  profile_false: require('./assets/ui/profile_false_icon.png'),

  translate_true: require('./assets/ui/translate_true_icon.png'),
  translate_false: require('./assets/ui/translate_false_icon.png'),

  diary_true: require('./assets/ui/diary_true_icon.png'),
  diary_false: require('./assets/ui/diary_false_icon.png'),

  music_true: require('./assets/ui/music_true_icon.png'),
  music_false: require('./assets/ui/music_false_icon.png'),

};

export default class AppContainer extends React.Component {


  constructor() {
    super();
    firebaseApp.auth().setPersistence('local');
    this.state = {
      appIsReady: false,
      currentUser: undefined,
      hasLocationPermission: false,
      location: undefined
    };
  }


  componentDidMount() {
    this._loadAssetsAsync();
    this.checkLocationPermission();
    this.registerForPushNotificationsAsync();
  }

  async _checkForUser() {
    if (firebaseApp.auth().currentUser) {
      console.log("GETTING ENTRIES")
      await getAllEntries(firebaseApp.auth().currentUser.uid)
      console.log("CHECKING LOCATION")
      this.checkLocation()
      console.log("MARKING AS LOGGED")
      this.setState({ currentUser: firebaseApp.auth().currentUser })
      this.setState({ appIsReady: true });
      return;
    }
    firebaseApp.auth().onAuthStateChanged((user) => {
      if (user) {
        console.log("GETTING ENTRIES")
        getAllEntries(firebaseApp.auth().currentUser.uid)
        console.log("CHECKING LOCATION")
        this.checkLocation()
        console.log("MARKING AS LOGGED")
        this.setState({ currentUser: user })
        this.setState({ appIsReady: true });
        return;
      } else {
        this.setState({ currentUser: undefined })
        this.setState({ appIsReady: true });
        return;
      }
    });
  }

  registerForPushNotificationsAsync = async () => {
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        return;
      }
      token = await Notifications.getExpoPushTokenAsync();
      //console.log(token);
      this.setState({ expoPushToken: token });
      this.sendToken();
    } else {
      alert('Must use physical device for Push Notifications');
    }

    if (Platform.OS === 'android') {
      Notifications.createChannelAndroidAsync('default', {
        name: 'default',
        sound: true,
        priority: 'max',
        vibrate: [0, 250, 250, 250],
      });
    }
  };

  async checkLocationPermission() {
    let { status } = await Location.requestPermissionsAsync();
    if (status !== 'granted') {
      setErrorMsg('Permission to access location was denied');
    } else {
      this.setState({ hasLocationPermission: true })
    }
  }

  sendToken() {
    if (this.state.expoPushToken) {
      console.log("TOKEN", this.state.expoPushToken)
      fetch(BACKEND_URL + '/notifications', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          token: this.state.expoPushToken
        }),
      }).then(response => {
        if (response.status === 201) {
          JSON.stringify(response.json().then(function (data) {
            console.log("TOKEN REPLY WORKED",data)
          }))
        } else {
          console.log(response.status)
        }
      });
    }else{
      console.log("No token to send")
    }
  }

  sendLocation() {
    fetch(BACKEND_URL + '/logs', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        latitude: this.state.location.coords.latitude,
        longitude: this.state.location.coords.longitude,
        timestamp: this.state.location.timestamp,
        user: firebaseApp.auth().currentUser.uid
      }),
    }).then(response => {
      if (response.status === 201) {
        JSON.stringify(response.json().then(function (data) {
          console.log(data)
        }))
      } else {
        JSON.stringify(response.json().then(function (data) {
          console.log(data)
        }))
      }
    });
  }

  checkLocation() {
    var self = this
    if (this.state.hasLocationPermission) {
      Location.hasServicesEnabledAsync().then(function (value) {
        console.log("HAS LOCATION?", value)
        if (value) {
          Location.getCurrentPositionAsync({ accuracy: Location.Accuracy.High }).then(function (location) {
            //console.log("LOCAITON", location)
            self.setState({ location: location })
            self.sendLocation()
          })
        }

      })

    }
  }
  async _loadAssetsAsync() {
    await Font.loadAsync({
      'Poppins-Regular': require('./assets/fonts/Poppins/Poppins-Regular.ttf'),
    })
    //_checkForUser sets the appIsReady flag so that the screen doesn't flicker between the slpash, login and home screens
    console.log("CHECK FOR USER")
    await this._checkForUser();

  }

  Home() {
    return (
      <Stack.Navigator>
        <Stack.Screen name="MainPage" component={Secured} />
        <Stack.Screen name="CameraScreen" component={CameraScreen} />
      </Stack.Navigator>
    );
  }

  Diary() {
    return (
      <Stack.Navigator>
        <Stack.Screen name="Diary" component={Diary} />
        <Stack.Screen name="CameraScreen" component={CameraScreen} />
        <Stack.Screen name="NewEntryScreen" component={NewEntryScreen} />
        <Stack.Screen name="RecordAudioScreen" component={RecordAudioScreen} />
      </Stack.Navigator>
    );
  }

  Profile(){
    return (
      <Stack.Navigator>
        <Stack.Screen name="Profile" component={Profile} />
        <Stack.Screen name="ReportBug" component={ReportBugScreen} />
        <Stack.Screen name="About" component={AboutScreen} />
      </Stack.Navigator>
    );
  }

  Music(){
    return(
      <Stack.Navigator>
        <Stack.Screen name="Music" component={Music} />
      </Stack.Navigator>
    )
  }

  render() {
    if (this.state.appIsReady) {
      if (this.state.currentUser) {
        return (
          <NavigationContainer>
            <Tab.Navigator screenOptions={({ route }) => ({
              tabBarIcon: ({ focused }) => {
                let tabIcon;
                if (route.name === 'Home') {
                  tabIcon = focused
                    ? tabIcons.translate_true
                    : tabIcons.translate_false;
                } else if (route.name === 'Diary' || route.name === 'NewEntryScreen') {
                  tabIcon = focused
                    ? tabIcons.diary_true
                    : tabIcons.diary_false;
                } else if (route.name === 'Profile') {
                  tabIcon = focused
                    ? tabIcons.profile_true
                    : tabIcons.profile_false;
                }
                else if (route.name === 'Music') {
                  tabIcon = focused
                    ? tabIcons.music_true
                    : tabIcons.music_false;
                }
                // You can return any component that you like here!
                return <Image style={styles.smallIcon} source={tabIcon} />;
              },
            })}
              tabBarOptions={{
                showLabel: false,
              }}
            >
              <Tab.Screen name="Home" component={this.Home} />
              <Tab.Screen name="Diary" component={this.Diary} />
              <Tab.Screen name="Profile" component={this.Profile} />
              <Tab.Screen name="Music" component={this.Music} />
              {/*<Tab.Screen name="CameraScreen" component={CameraScreen} />*/}
            </Tab.Navigator>
          </NavigationContainer>
        )
      } else {
        return (
          <NavigationContainer>
            <Stack.Navigator>
              <Stack.Screen name="SignInScreen" component={SignIn} />
              <Stack.Screen name="SignUpScreen" component={SignUp} />
            </Stack.Navigator>
          </NavigationContainer>
        )
      }
    }
    else {
      return <WelcomePage />
    }
  }
}

const styles = StyleSheet.create({
  smallIcon: {
    width: 30,
    height: 30,
  }
});