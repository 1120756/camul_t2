# README #

This is the repository for the development of the FigureOut apps, a mobile diary with features like text to sign language translation, text to speech, and much much more!

- [Project Wiki](https://bitbucket.org/RicGD/camul1920/wiki/Home)

The team is as follows:

- ***Project Manager***
    - Gil Durão

- ***Designers***
    - Margarida Guerra
    - Nelson Sousa

- ***Developers***
    - Luís Costa (Backed)
    - João Espinheira (Backend)
    - Johann Knorr (Frontend)
    - Pedro Pinho (Backend)
    - Tiago Faria (Frontend)

- ***Testers***
    - Damien Barros
    - Flávio Costa
    - João Magalhães

- ***Content Expert***
    - Damien Barros